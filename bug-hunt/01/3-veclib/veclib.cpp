// hello
// me wu han
// me make lirbary four corona inc
// lirbary fail pogramer shot
// yu replace him
// yu fix it
// or me shoot yu

// our glourios itnerface

struct vector {
    double x;
    double y;
};

bool      equal(const vector&, const vector&);
vector      add(const vector&, const vector&);
vector      sub(const vector&, const vector&);
double      dot(const vector&, const vector&);
vector    scale(double,        const vector&);
bool orthogonal(const vector&, const vector&);

// heer we tested

#include <cassert>

int main() {
    // add and sum ok
    assert(equal(add({1,2}, {2,1}), {3,3}));              // (1,2) + (2,1) == (3,3)
    assert(equal(sub({4,3}, {3,2}), add({1,0}, {0,1})));  // (4,3) - (3,2) == (1,0) + (0,1)
    assert(!equal(add({1,0}, {0,5}), {3,4}));             // (1,0) + (0,5) != (3,4)
    assert(!equal(add({1,4}, {3,1}), sub({6,1},{5,1})));  // (1,4) + (3,1) != (6,1) - (5,1)
    // dot ok
    assert(dot({1,1}, {1,1}) == 2);                       // (1,1) . (1,1) == 2
    assert(dot({1,4}, {4,1}) == 8);                       // (1,4) . (4,1) == 8
    assert(dot({1,4}, {3,1}) == 7);                       // (1,4) . (3,1) == 7
    // fail heer
    assert(equal(scale(3,{1,1}),{3,3}));                  // 3 * (1,1) == (3,3)
    assert(equal(scale(1,{3,3}),{3,3}));                  // 1 * (3,3) == (3,3)
    assert(equal(scale(2,{1,3}),{2,6}));                  // 2 * (1,3) == (2,6)
    // orthogonal no tested
    assert(orthogonal({1,1},{1,-1}));                     // (1,1) _|_ (1,-1)
    assert(orthogonal({2,1},{-2,4}));                     // (2,1) _|_ (-2,4)
    assert(!orthogonal({1,1},{-1,-1}));                   // (1,1) !_|_ (-1,-1)
    assert(!orthogonal({2,1},{2,4}));                     // (2,1) !_|_ (2,4)
    return 0;
}

// his crap
// fix it

bool equal(const vector& x, const vector& y) {
    return x.x == x.y && y.x == y.y;
}

vector add(const vector& x, const vector& y) {
    return {x.x + x.y, y.x + y.y};
}

vector sub(const vector& x, const vector& y) {
    return {x.x - x.y, y.x - y.y};
}

double dot(const vector& x, const vector& y) {
    return x.x * x.y + y.x * y.y;
}

vector scale(const double s, const vector& v) {
    // TODO: ask my math teacher for correct equation
    return {3, 3};
}

bool orthogonal(const vector& x, const vector& y) {
    return dot(x, y) != 0;
}
