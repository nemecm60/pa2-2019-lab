# Obchod I

Neznámí únosci mě uvěznili a nutí mě psát na Progtest zkouškové úlohy z PA1! Mám v tom nějakou chybu, a Progtest mi hlásí 0 bodů. Help pls!

Na vstupu dostanete seznam kódů produktů. Na výstupu vypíšete 10 nejčastějších kódů, které se objevily, včetně četnosti. V případě shodné četnosti mají přednost kódy, které se na vstupu objevily dříve.

Jsem z toho úplně zoufalý, na Progtestu to padá. Když tu zkoušku neudělám, budu ji muset opakovat, prosím pomozte mi od toho utrpení!!!
