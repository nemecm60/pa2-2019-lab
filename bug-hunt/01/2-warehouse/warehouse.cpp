#include <string>
#include <iostream>

struct Item {
    std::string ean;
    unsigned int qty;
};

struct ItemArray {
    Item * items;
    unsigned int capacity;
    unsigned int count;
};

void addItem(ItemArray items, std::string ean) {
    if(items.capacity == items.count) {
        items.capacity *= 2;
        Item * newItems = new Item[items.capacity];
        for(auto i = 0u; i < items.count; ++i) {
            newItems[i] = items.items[i];
        }
        delete items.items;
        items.items = newItems;
    }
    items.items[items.count].ean = ean;
    items.items[items.count].qty = 1;
    items.count++;
}

Item* findItem(ItemArray items, std::string ean) {
    for(auto i = 0u; i < items.count; ++i) {
        if(ean == items.items[i].ean) { // This comparison is OK. Trust me, I am an engineer.
            return items.items + i;
        }
    }
    return nullptr;
}

Item* findTopItem(ItemArray items) {
    Item* topItem = nullptr;
    for(auto i = 0u; i < items.count; ++i) {
        if(items.items[i].qty > 0 && (!topItem || items.items[i].qty > topItem->qty)) {
            topItem = items.items + i;
        }
    }
    return topItem;
}

constexpr ItemArray createItemArray() {
    return {
        nullptr,
        0,
        0
    };
}

void destroyItemArray(ItemArray items) {
    delete items.items;
}

int main() {
    auto items = createItemArray();
    std::cout << "Kody:" << std::endl;
    while(!std::cin.eof()) {
        std::string line;
        std::getline(std::cin, line);
        if(line.empty()) break;
        if(auto item = findItem(items, line)) {
            item->qty += 1;
        }
        else {
            addItem(items, line);
        }
    }
    std::cout << "Top produkty:" << std::endl;
    for(auto i = 0u; i < 10; ++i) {
        if(auto item = findTopItem(items)) {
            std::cout << item->ean << ": " << item->qty << "x" << std::endl;
            item->qty = 0;
        }
    }
    destroyItemArray(items);
    return 0;
}
