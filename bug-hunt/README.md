# Semestrální bug hunt

I ten nejlepší programátor dělá chyby. To je naprosto v pořádku. Je však důležité naučit se chyby hledat a opravovat. Proto jsme zařadili (jako takový drobný experiment v rámci výuky PA2) tuto disciplínu. Za řešení bug huntu jsou virtuální body za aktivitu, podobně jako za řešení úloh na samostatnou práci.

## Nástroje k hledání chyb

Intuice, zkušenost, znalosti... ale také paměťový debugger (valgrind), normální debugger (gdb), a všechny hlášky warning z kompilátoru (ať už jde o pedantický g++, nebo clang-tidy). Můžete také potřebovat tvořit vlastní vstupy, které povedou k chybám.

Obvykle bývají chyby jednoduché, jak to bývá s každým vyřešeným hlavolamem, ale je důležité je najít a správně opravit.

## Ke kvalitě / realističnosti kódu

Zdrojovými kódy v této sekci se příliš neinspirujte. Jsou sice dobrou ukázkou použití jednotlivých probíraných konceptů, nicméně v mnoha případech půjde o kód, který by rozumný programátor nenapsal, nebo který je příliš podivný a zvláštní. Jako celek takový kód nemusí být dobrou inspirací. Nicméně praxe je taková, že programátoři jsou opravdu různých kvalit; byť jsou zadání bug huntu smyšlená, mnohdy vycházejí ze skutečných bugů, na které jsme narazili, jak u sebe, tak u našich kolegů či ve veřejných repozitářích a různých legendách.

Budou zde však všechny možné druhy chyb -- špatná práce s pamětí, chyby v kompilaci, logické chyby atd. V pozdějších týdnech také můžeme chtít najít pomalá místa v kódu a zrychlit je.

## Co získám řešením bug huntů?

Především dobrý čich na kód obsahující chyby (tzv. _bad code smell_). Zkušenost s hledáním a opravováním chyb. A také jistou zkušenost s tím, jak kód raději nepsat, a naopak jak jej psát tak, aby se riziko výskytu chyb minimalizovalo. A hlavně si budete počínat jistěji u zkoušky.

## Jak zasílat řešení

Opravy realizujte ve svých repozitářích, v nich poté vytvořte merge request a pošlete nám na něj odkaz.