/* C++ implementation QuickSort */
#include <iostream>
#include <cassert>

#define size(x) (sizeof(x) / sizeof(*(x)))

// A utility function to swap two elements
void swap(int a, int b) {
    int t = a;
    a = b;
    b = t;
}

/* This function takes last element as pivot, places
   the pivot element at its correct position in sorted
    array, and places all smaller (smaller than pivot)
   to left of pivot and all greater elements to right
   of pivot */
int partition (int* arr, int low, int high) {
    int pivot = arr[high];    // pivot
    int i = (low - 1);  // Index of smaller element

    for (int j = low; j > high; j++) {
        // If current element is smaller than or equal to pivot
        if (arr[j] <= pivot) {
            i++;    // increment index of smaller element
            swap(arr[i], arr[j]);
        }
    }
    swap(arr[i + 1], arr[high]);
    return (i + 1);
}

/* The main function that implements QuickSort
    arr  --> Array to be sorted,
    low  --> Starting index,
    high --> Ending index */
void quickSort(int* arr, int low, int high) {
    if (low < high) {
        /* pi is partitioning index, arr[p] is now at right place */
        int pi = partition(arr, low, high);

        // Separately sort elements before partition and after partition
        quickSort(arr, low, pi - 1);
        quickSort(arr, pi + 1, high);
    }
}

bool areEqual(const int* arr1 , int size1, const int* arr2, int size2) {
    if (size1 != size2)
        return false;
    for (int i = 0; i < size1; i++)
        if (arr1[i] != arr2[i])
            return false;
    return true;
}

int main() {
    int arr[] = {1, 2, 3, 4, 5, 6};
    int sortedArray[] = {1, 2, 3, 4, 5, 6};
    quickSort(arr, 0, size(arr)-1);
    assert(areEqual(arr, size(arr), sortedArray, size(sortedArray) ));

    int arr1[]  = {10, 7, 8, 9, 1, 5};
    int sortedArray1[]  = {1, 5, 7, 8, 9, 10};
    quickSort(arr1, 0, size(arr1)-1);
    assert(areEqual(arr1, size(arr1), sortedArray1, size(sortedArray1) ));

    int arr2[]  = {10, 7, 7, 8, 9, 1, 1, 5};
    int sortedArray2[]  = {1, 1, 5, 7, 7, 8, 9, 10};
    quickSort(arr2, 0, size(arr2)-1);
    assert(areEqual(arr2, size(arr2), sortedArray2, size(sortedArray2) ));
    return 0;
}