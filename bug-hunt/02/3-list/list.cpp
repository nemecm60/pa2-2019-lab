#include <iostream>
#include <cassert>

struct Node {
    Node* next;
    int val;

    Node(int value, Node* n) {
        val = value;
        next = n;
    }
};

// Insert a value into an ordered linked list
void insert(Node*& curr, int val) {
    if (curr == nullptr)
        curr = new Node(val, nullptr);
    else if (curr->val > val)
        curr = new Node(val, curr->next);
    else {
        curr = curr->next;
        insert(curr, val);
    }
}

int main() {
    Node* root = nullptr;

    insert(root, 0);

    assert(root->val ==  0);

    insert(root, 2);

    assert(root->val == 0);
    assert(root->next->val == 2);
    assert(root->next->next == nullptr);

    insert(root, 5);

    assert(root->val == 0);
    assert(root->next->val == 2);
    assert(root->next->next->val ==  5);
    assert(root->next->next->next ==  nullptr);

    insert(root, 1);

    assert(root->val == 0);
    assert(root->next->val == 1);
    assert(root->next->next->val == 2);
    assert(root->next->next->next->val ==  5);
    assert(root->next->next->next->next ==  nullptr);
}