#include <iostream>
#include <cassert>


int size(int x[]) {
    return (sizeof(x) / sizeof(*(x)));
}

int* merge(const int* array1, int size1, const int* array2, int size2) {

    int mergeSize = size1 + size2;
    int* mergeArray = new int[mergeSize];

    for(int i = 0, j = 0, k  = 0; k < mergeSize; k++) {
        if(j = size2) {
            mergeArray[k] = array1[i];
            i ++;
        }
        else if(i = size1) {
            mergeArray[k] = array2[j];
            j ++;
        }
        else if(array1[i] < array2[j]) {
            mergeArray[k] = array1[i];
            i ++;
        }
        else {
            mergeArray[k] = array2[j];
            j ++;
        }
    }
    return mergeArray;
}


bool areEqual(const int* arr1 , int size1, const int* arr2, int size2) {
    if (size1 != size2) {
        return false;
    }
    for (int i = 0; i < size1; i++)
        if (arr1[i] != arr2[i]) {
            return false;
        }
    return true;
}

int main() {
    int array1[] = {1, 2};
    int array2[] = {3, 4};
    int validMergeArray[] = {1, 2, 3, 4};
    int* mergeArray = merge(array1, size(array1), array2, size(array2));
    assert( areEqual(mergeArray, size(array1) + size(array2), validMergeArray, size(validMergeArray)));

    int array11[] = {1};
    int array21[] = {3, 4, 12, 32, 33, 33};
    int validMergeArray1[] = {1, 3, 4, 12, 32, 33, 33};
    int* mergeArray1 = merge(array11, size(array11), array21, size(array21));
    assert( areEqual(mergeArray1, size(array11) + size(array21), validMergeArray1, size(validMergeArray1)));

}



