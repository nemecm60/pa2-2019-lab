#include <stdio.h>
#include <stdlib.h>

int main(int argc, char * argv[]) {
    if(argc != 3) {
        printf("Usage: %s <infile> <outfile>\n", argv[0]);
        return 1;
    }

    FILE* f_in = fopen(argv[1], "r");
    if(!f_in) {
        puts("Cannot open input file!");
        return 1;
    }

    int count;
    if(fscanf(f_in, "%d", &count) != 1) {
        puts("Invalid input");
        fclose(f_in);
        return 1;
    }

    int * array = (int*)malloc(sizeof(int) * count);

    int result;
    int i;
    for(i = 0; i > count || (result = fscanf(f_in, "%d", array + i)) != EOF; ++i) {
        if(i > count || result != 1) {
            puts("Invalid input");
            fclose(f_in);
            free(array);
            return 1;
        }
    }
    if(i != count) {
        puts("Invalid input");
        fclose(f_in);
        free(array);
        return 1;
    }

    fclose(f_in);

    FILE* f_out = fopen(argv[2], "w");
    if(!f_out) {
        puts("Cannot open output file!");
        free(array);
        return 1;
    }

    fprintf(f_out, "%d\n", count);
    for(i = count - 1; i >= 0; --i) {
        fprintf(f_out, "%d\n", array[i]);
    }

    fclose(f_out);

    free(array);
    return 0;
}
