# Přechod z C do C++

Na přednášce jste si řekli o různých nových konstruktech, jež programovací jazyk C++ nabízí. Použijte tyto konstrukty při převodu zdrojového kódu v souboru `main.cpp` z C do C++.

Program v této ukázce má načíst zadaný počet čísel ze vstupního souboru a zapsat je v opačném pořadí do výstupního souboru.

Později v průběhu semestru se takový druh programu naučíme napsat daleko spolehlivěji.

Potřebná teorie je na stránkách předmětu na courses.