# Přechod z C do C++

Na přednášce jste si řekli o různých nových konstruktech, jež programovací jazyk C++ nabízí. Použijte tyto konstrukty při převodu zdrojového kódu v souboru `main.cpp` z C do C++.

Většina teorie je na stránkách předmětu na courses.

Zde bych (Honza) rád zmínil/doplnil několik dalších užitečných konstrukcí, případně rozšířil látku o další informace:

## Jmenné prostory

Jazyk C měl pouze jeden (globální) jmenný prostor. Pokud jste definovali nějakou funkci/typ/proměnnou, riskovali jste, že s novou normou nějaký zmetek (s mnoha tituly, velkým vlivem v normalizační komisi, a mrakem dobrých úmyslů) přidá do standardních hlaviček funkci/typ/proměnnou se stejným názvem, a rozbije Vám kód.

V jazyce C++ jsou proto zavedeny jmenné prostory, jež se značí klíčovým slovem `namespace`, a které Vám umožní pojmenování konstruktů ve Vašem kódu rozčlenit. Podobnou funkci jako jmenné prostory mají i struktury (`struct`/`class`). K rozlišení stejně pojmenovaných konstruktů v různých jmenných prostorech slouží tzv. rozlišovací (resolution) operátor `::`

Všechny standardní knihovny v jazyce C++ potom definují veškeré své konstrukce ve jmenném prostoru `std`. Jiné knihovny třetích stran budou mít svoje jmenné prostory; pokud je nemají, koledují si jejich autoři a uživatelé dříve či později o průser.

Následuje ukázka práce se jmennými prostory:

```c++
namespace myGreatFunctions {
    // ve jmenném prostoru myGreatFunctions definuji funkci max
    int max(int a, int b) {
        return a > b ? a : b;    
    }
}

namespace otherWrongFunctions {
    // ve jmenném prostoru otherWrongFunctions definuji jinou funkci max
    int max(int a, int b) {
        return a + b;    
    }
}

int main() {
    // zavolám funkci max ze jmenného prostoru myGreatFunctions
    int c = myGreatFunctions::max(4,5);
    return c;
}

// tentýž jmenný prostor mohu definovat kolikrát chci; těla těchto prostorů se spojí do jednoho
// (toto nelze dělat se strukturami/třídami (ke škodě jazyka C++))
namespace otherWrongFunctions {
    // toto by způsobilo chybu v kompilaci, protože funkce max už je v tomto jmenném prostoru definována
    // int max(int a, int b) { return 0; }

    int sum(int a, int b) {
        // k volání funkce ze svého jmenného prostoru nemusím použít rozlišovací operátor
        return max(a, b);
    }
    
    // jmenné prostory mohu vnořovat
    // pak mohu potřebovat více ::
    namespace myGreatFunctions {
        // toto je úplně jiná funkce v úplně jiném prostoru než na začátku ukázky
        int max(int a, int b) {
            // zavoláme funkci max z nadřazeného prostoru
            // cestu musím uvést, jinak bych páchal rekurzi
            return otherWrongFunctions::max(a, b);
        }
        int max2(int a, int b) {
            // zavoláme funkci sum z nadřazeného prostoru
            // cestu uvádět nemusím, protožou se automaticky hledá symbol v:
            // 1) tomto prostoru
            // 2) rodičovském prostoru
            // 3) jeho rodičovském prostoru
            // atd.
            // je to stejné jako s překrýváním proměnných v blocích 
            return sum(a, b);
        }
        int maxCorrect(int a, int b) {
            // zavoláme funkci max z prostoru na začátku ukázky
            // zde musím použít absolutní cestu, :: na začátku mi ukáže na globální jmenný prostor
            return ::myGreatFunctions::max(a, b);
        }
        // mohu samozřejmě pouze deklarovat hlavičku funkce...
        int maxSomewhereElse(int a, int b);
    }
}

// a její obsah definovat kdekoliv jinde
// v takovém případě ji musím uvést se správnou cestou
int otherWrongFunctions::myGreatFunctions::maxSomewhereElse(int a, int b) {
    // uvnitř těla funkce se nacházíme uvnitř jmenného prostoru, ve kterém je funkce deklarována
    // v tomto případě tedy max znamená funkci otherWrongFunctions::myGreatFunctions::max
    return max(a, b);
}
```

Lze rovněž vytvořit jmenný prostor jako alias jiného jmenného prostoru. Aliasovat lze i vnořené namespacy (jak asi správně tušíte, prostě na pravé straně uvedete příslušnou cestu s `::`).

```c++
#include <iostream>

namespace matouj10_original_library = std;

int main() {
    matouj10_original_library::cout << "Matouškova knihovna je lepší než STL!" << matouj10_original_library::endl;
    return 0;
}

```

Můžeme chtít nahrát všechny (nebo některé) symboly z nějakého jmenného prostoru do aktuálního bloku, abychom nemuseli používat `::`. K tomu se používá příkaz `using`.

```c++
namespace x {
    int max(int a, int b) {
        return a > b ? a : b;    
    }
}

int main() {
    using namespace x;
    // nyní můžeme použít x::max, jako by byl v našem aktuálním jmenném prostoru
    // ovšem pouze mezi aktuálními { }
    return max(1,2);
}

namespace y {
    int min(int a, int b) {
        return a < b ? a : b;    
    }
}

using namespace y;

// odteď můžeme v celém zbytku kódu použít funkci min bez psaní y::
// toto silně nedoporučuji, a už vůbec ne v hlavičkovém souboru
// existují dva druhy firem
// v jednom si koledují o průser v případě kolize symbolů v prostorech
// v druhém k tomu průseru došlo
// nebuďte ale smutní... k těm druhým firmám patří např. Microsoft

// místo toho radím použít konkrétní symboly ze jmenných prostorů, např.:
using x::max;

// taky je možno (a je to ok) použít konkrétní symbol pod svým aliasem, např.:
using str = std::string; 
// lze též: typedef std::string str; - problém ale nastane později s šablonami
```

Já osobně nepoužívám konstrukce typu `using namespace std;`, neboť tím přicházím o výhody jmenných prostorů. Psaní `std::` není zas taková cena za kód, který je odolnější vůči nečekaným budoucím rozšířením jmenných prostorů.

Klíčové slovo `using` má i jiná využití, těm se ale věnovat nebudeme.

Svůj jmenný prostor může nově definovat i výčtový typ, aby hodnoty výčtového datového typu nezamořovaly jmenný prostor, ve kterém je výčtový typ definován.

```c++
// jednotlivé hodnoty budou A, B, C, což nemusí být vždy vhodné
enum T1 {
    A,
    B,
    C
};

// jednotlivé hodnoty budou T1::A, T1::B, T1::C, názvy tedy budou hezky pohromadě s výčtovým typem
// místo enum struct lze též napsat enum class, význam je zcela identický
// drobná (ne)výhoda: s hodnotami takového výčtového typu již nelze pracovat jako s čísly
enum struct T1 {
    A,
    B,
    C
};
```

## Inline

Klíčovým slovem `inline` se původně označovaly funkce, jejichž tělo měl překladač vložit rovnou místo volání funkce, a tím ušetřit manipulaci se zásobníkem a jinou režii související s voláním funkce. V dnešní době se toto slovo použité v tomto smyslu zcela ignoruje, a překladače inlinují funkce podle svého uvážení a nastaveného stupně optimalizace.

Slovo `inline` má i jiná použití, nebudeme se jim však v PA2 věnovat.

## Nullptr

Při přetěžování funkcí můžeme narazit na případ, kdy jedna varianta bude přijímat pointer, a druhá varianta bude přijímat celé číslo. Jak předat nulový pointer?

```c++
void f(int a) {}
void f(char* a) {}

int main() {
    f(0);
    return 0;
}
```

V uvedeném případě se vybere varianta s parametrem typu `int`. Ale co když jsme zamýšleli předat nulový pointer? Zkusíme konstantu `NULL`:

```c++
int main() {
    f(NULL);
    return 0;
}
```

Nepůjde zkompilovat. Zkusíme vlastní univerzální nulový pointer:

```c++
int main() {
    f((void*)0);
    return 0;
}
```

Nepůjde zkompilovat (schválně se někdy podívejte, přesně tak je definován `NULL`). Jediné, co projde, je použít nulový pointer daného typu:

```c++
int main() {
    f((char*)0);
    return 0;
}
```

Toto již projde. Ale zcela se ztrácí smysl přetěžování funkcí; navíc musíme řešit typ předávaného pointeru.

Proto v C++ díky typové magii existuje speciální pointer `nullptr`. Jde o hodnotu speciálního typu `nullptr_t`, která je implicitně převoditelná na kterýkoliv pointer, ale nelze ji implicitně převést na číslo. Překladač tedy nevybere pro `nullptr` variantu funkce s číselným parametrem.

```c++
int main() {
    f(nullptr);
    return 0;
}
```

Naučte se, pokud potřebujete funkci předat nulový pointer, vždy použít `nullptr`. V mnoha programech způsobovalo (a způsobuje) použití starších konstant zajímavé bugy.

Při přiřazování hodnot do proměnné, vracení z funkce apod. není k užívání `nullptr` důvod, ale konzistentní použití `nullptr` pro nulový pointer všude samozřejmě vůbec neuškodí, a podporuje to správné programátorské návyky.

## Auto

Toto klíčové slovo se probírá až na konci semestru. Je však lehce pochopitelné už teď. Kompilátor může odvodit typ definované proměnné z hodnoty, jež nabývá. Takové schopnosti se říká _type inference_, a některé programovací jazyky, jako např. Scala, ji dovádějí do značných extrémů.

Jazyk C++ umožňuje použít klíčové slovo `auto` jako název typu, a to pouze při definici proměnné (nikoliv člen struktury a nikoliv parametr funkce), které je hned v této definici přiřazena nějaká hodnota.

```c++
int a = 0;
auto b = a; // b bude typu int

// mějme někde definovanou funkci s touto hlavičkou...
very_long_type_in_many_namespaces f();

auto x = f();

// auto y; // toto nebude fungovat, protože se nepřiřazuje žádná hodnota, a tedy neznáme její typ
// int max(auto a = 7, auto b = 0) // taky nebude fungovat, byť přiřazujeme výchozí hodnoty
// auto min(int a, int b) { return a > b ? a : b; } // něco takového projde v jazyce Scala, ale ne C++
```

Klíčové slovo `auto` samozřejmě můžeme podle potřeby kombinovat s referencemi a klíčovým slovem `const`. (Není problém, když se tímto způsobem potkají dva `const` modifikátory vedle sebe.)

Možná se ptáte, jaký typ bude mít proměnná, když se jí přiřadí třeba číslo. Typy literálů jsou však jasně definované, a měli byste je znát z PA1:

```c++
auto a = 0; // a bude int
auto a = 0u; // a bude unsigned int
auto a = 0l; // a bude long
auto a = 0.0; // a bude double
auto a = 0.f; // a bude float
auto a = 'x'; // a bude char
auto a = "x"; // a bude const char *
auto a = true; // a bude bool
// pozor:
auto a = NULL; // a bude void*, se všemi problémy, které z toho poplynou
auto a = nullptr; // a bude nullptr_t, nikdy do něj nepřiřadíte nenulový pointer
auto a = {1, 2, 3}; // a bude std::initializer_list<int>, nikoliv int[]
```

## Constexpr

Toto klíčové slovo se v předmětu PA2 vůbec neprobírá, a já jej rozhodně nebudu nikde požadovat. Bylo by však dobré vědět, že něco takového existuje.

V jazyce C se k definování konstant používala direktiva preprocesoru `#define`. S makry se však špatně pracuje, a vývoj jazyka C++ postupně směřuje ke zcela bezpreprocesorové existenci (ve verzi C++20 zmizí potřeba i pro poslední dosud nezbytnou direktivu `#include`).

Konstantu s `#define` můžete nahradit pomocí globální `const` proměnné, a dokonce získáte řadu výhod (např. ji můžete schovat do jmenného prostoru).

Jedním problémem je, že vytváříte proměnnou, která bude po kompilaci zapsána v datovém segmentu programu, a při práci s ní se bude do tohoto segmentu přistupovat (ale překladač může tuto záležitost optimalizovat). To někdy může vadit.

Klíčové slovo `constexpr` funguje podobně jako modifikátor `const`, ale navíc způsobí výpočet daného symbolu kompilátorem; ten potom místo, kde se použije taková proměnná nebo funkce, nahradí svým vypočítaným výsledkem. Proměnná/funkce pak nebude ve výsledném kódu existovat (pokud nebude volána s parametry, jejichž hodnotu překladač nezná, nebo pokud nebude někde předávána adresa takové proměnné či funkce).

Některé syntaktické konstrukce jazyka vyžadují zadání konstanty, jejíž hodnota je známa při překladu. V těchto případech použití klíčového slova `const` nepostačuje. Klíčové slovo `constexpr` umožňuje definovat konstanty vyhodnocené při překladu, a funkce, jež takové konstanty při překladu vypočítají. Funkce s modifikátorem `constexpr` mají svá omezení, asi těžko budou pracovat se vstupem či výstupem apod. Typicky taková funkce bude při zpracování kompilátorem vracet hodnotu nějakého skalárního typu nebo jednoduchou strukturu či statické pole; už ne však dynamicky alokovanou paměť.

```c++
constexpr int fib(int n) {
    if(n <= 2) return 1;
    return fib(n - 2) + fib(n - 1);
}

constexpr int dim = 10;

int main() {
    // velikost pole vyplněného nulami musí být známa při překladu, fib i dim tedy musí být constexpr
    int arr[fib(dim)] = { };
    // atd...
}
```

Mimochodem, výše uvedený příklad s pomocí maker ani zapsat nelze (makra nepodporují rekurzi).

Reálným případem užití `constexpr` může být předvýpočet převodních a kódovacích tabulek, a to zejména v případě, kdy by jejich výpočet za běhu programu byl příliš zdlouhavý (např. pokud bude program spuštěn na velmi pomalém a omezeném stroji). Dalším častým případem jsou `constexpr` konstruktory.

Je třeba míti na paměti, že užívání `constexpr` zpomaluje kompilaci a v případě užití ke generování tabulek celkem logicky zvětšuje velikost výsledného programu. 