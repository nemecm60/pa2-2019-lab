// (1) Use C++ headers instead of old C headers.

#include <stdio.h>
#include <stdlib.h>

#define MIN(x,y) ((x)<(y)?(x):(y))
#define ANSWER_TO_LIFE_UNIVERSE_AND_EVERYTHING MIN(6*9,42)

typedef int wagon;
#define WAGON_LOCO   1
#define WAGON_PEOPLE 2
#define WAGON_WOOD   3
#define WAGON_COAL   4

// (2) Get rid of the struct keyword in a variable type name.

struct train {
    wagon type;
    struct train * next;
};

// (3) The following is a helper function; we should move it to a separate namespace. Also, inline the function.

const char * wagon_str(wagon w) {
    switch(w) {
        case WAGON_LOCO:
            return "<:^===^:>";
        case WAGON_PEOPLE:
            return "[:8:::8:]";
        case WAGON_WOOD:
            return ";=|===|=;";
        case WAGON_COAL:
            return "[_______]";
        default:
            return "WTF?";
    }
}

// (4) Rename the following function just to print.

void print_wagon(wagon w) {
    printf("%s", wagon_str(w));
}

void print(struct train * t) {
    if(t) {
        print_wagon(t->type);
        print(t->next);
    }
    else {
        printf("\n");
    }
}

struct train * createTrain(wagon w, int count) {
    if(count < 1) return 0;
    struct train * t = (struct train *)malloc(sizeof(struct train));
    t->type = w;
    t->next = createTrain(w, count - 1);
    return t;
}

// (5) Rename the following function to createTrain (so it can take arguments in any order)

struct train * createTrain2(int count, wagon w) {
    return createTrain(w, count);
}

void freeTrain(struct train * t) {
    if(t) {
        freeTrain(t->next);
        free(t);
    }
}

// (6) Following two functions have an ugly interface; make the train parameter a single pointer.

void removeWagon(struct train ** t) {
    if(*t) {
        struct train * tmp = *t;
        *t = (*t)->next;
        free(tmp);
    }
}

// (7) The following function should have a same name as the previous function.

void removeWagonMultiple(struct train ** t, int count) {
    if(count > 0 && *t) {
        struct train * tmp = *t;
        *t = (*t)->next;
        free(tmp);
        removeWagonMultiple(t, count - 1);
    }
}

// (8) Use proper data type for logic expressions.

int isTrainEmpty(struct train * t) {
    return !t;
}

int main() {
    struct train * t1 = createTrain(WAGON_LOCO, 3);
    print(t1);
    while(!isTrainEmpty(t1)) {
        removeWagon(&t1);
        print(t1);
    }
    freeTrain(t1);

    struct train * t2 = createTrain2(ANSWER_TO_LIFE_UNIVERSE_AND_EVERYTHING, WAGON_COAL);
    print(t2);
    while(!isTrainEmpty(t2)) {
        removeWagonMultiple(&t2, 10);
        print(t2);
    }
    freeTrain(t2);

    print(0);
}

// (9) Get rid of stdlib, use new/delete operators
// (10) Get rid of stdio, use streams instead of printf
// (11) Get rid of #define's
// (12) Use auto keyword when possible