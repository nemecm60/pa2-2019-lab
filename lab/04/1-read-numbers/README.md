# Základní práce s `std::vector`

Zadáním je naimplementovat program, který načte na vstupu čísla, a poté je vypíše pozpátku.

Řadu čísel zakončíme číslem 0.

Vyzkoušíme si:

* `vector` jako nafukovací pole.
* Výpis vektoru pomocí cyklu, obyčejně.
* Výpis vektoru popředu pomocí ranged for.
* Výpis vektoru popředu pomocí iterátorů.
* Výpis vektoru pozadí pomocí iterátorů.
* Výpis vektoru pozadu pomocí ranged for. (*)
* Prohození prvků pomocí algoritmu `reverse`.
* Seřazení vektoru pomocí algoritmu `sort`.
* Nalezení prvku v seřazeném vektoru, různými způsoby.

## Doporučené zdroje

* https://en.cppreference.com/w/cpp/container/vector
* https://en.cppreference.com/w/cpp/algorithm/reverse
* https://en.cppreference.com/w/cpp/algorithm/sort

## Další zdroje

* https://en.cppreference.com/w/cpp/iterator/iterator
* https://en.cppreference.com/w/cpp/algorithm/stable_sort