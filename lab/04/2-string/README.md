# Základní práce s `std::string`

Zadáním je naimplementovat program, který určí, zda dva načtené řetězce jsou shodné. V případě, že nejsou shodné, řetězce spojte a zakódujte Caesarovou šifrou o zadaný počet pozic.

Řadu čísel zakončíme číslem 0.

Vyzkoušíme si:

* `string` jako lepší `const char *`.
* Porovnání dvou řetězců.
* Spojování dvou řetězců.
* Výpis zašifrovaného řetězce pomocí cyklu for.
* Výpis zašifrovaného řetězce pomocí funkce `for_each`.
* Zašifrování řetězce pomocí `transform`.

## Doporučené zdroje

* https://en.cppreference.com/w/cpp/algorithm/for_each
* https://en.cppreference.com/w/cpp/algorithm/transform
