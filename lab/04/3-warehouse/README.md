# `std::vector` na struktuře, vkládání do vektoru

Úkolem je vytvořit program, který bude provádět transakce se zbožím ve skladu.

Na vstupu programu je seznam transakcí se skladovaným zbožím. Transakcí je naskladnění (dodání na sklad) a vyskladnění (odepsání ze skladu). Transakce se liší prvním znakem (`+` nebo `-`). Následuje naskladněný/vyskladněný počet kusů zboží a EAN kód zboží, který zboží jednoznačně identifikuje. Vlastní EAN kód je posloupnost desítkových cifer, celková délka je nejméně 5 a nejvýše 100 cifer (reálný EAN kód musí být kratší, délka 100 je zvolena pro tuto úlohu). Zadávání transakcí skončí s koncem standardního vstupu (EOF).

Program načtené operace průběžně vyhodnocuje. Výstupem programu je počet kusů zboží daného EAN kódu po provedení požadované transakce. Výsledkem transakce je:

* naskladnění, tedy přidání zboží na sklad. Výsledkem je upravený počet kusů zboží ve skladu,
* úspěšné vyskladnění, tedy odebrání zboží ze skladu. Výsledkem je upravený počet kusů zboží ve skladu. Operaci lze provést pouze pokud je na skladu dostatečný počet kusů zboží,
* neúspěšné vyskladnění, kdy zboží ze skladu není odebráno, protože jej na skladě není dostatečné množství. Program zobrazuje odlišný výstup, stav skladu se nezmění a pokračuje zpracováním další transakce.

Pokud je vstup neplatný, program to musí detekovat a zobrazit chybové hlášení. Chybové hlášení zobrazujte na standardní výstup (ne na chybový výstup). Za chybu považujte:

* transakce není ani naskladnění `+` ani vyskladnění `-`,
* chybí množství naskladňovaného/vyskladňovaného zboží nebo počet kusů zboží je nulový nebo záporný,
* chybí znak `x` za počtem kusů,
* EAN kód obsahuje znaky navíc kromě desítkových číslic,
* EAN kód je příliš krátký (kratší než 5 cifer) nebo příliš dlouhý (delší než 100 cifer).

Ukázka práce programu:

```
Operace:
+ 5x 12345
> 5x
- 2x 12345
> 3x
- 7x 12345
> Nedostatecne zasoby
+ 4x 12345
> 7x
- 2x 12345
> 5x
+ 2x 084212
> 2x
+ 1x 354357
> 1x
+ 2x 2345123
> 2x
+ 1000x 4534573
> 1000x
- 3x 5674523
> Nedostatecne zasoby
- 1000x 4534573
> 0x
- 10x 2345123
> Nedostatecne zasoby
- 1x 2345123
> 1x
```

```
Operace:
* 7x 1234545678
Nespravny vstup.
```

```
Operace:
+ 2x 12345
> 2x
- 5x invalid
Nespravny vstup.
```

# Pokyny k vypracování

* Nadefinujte vhodné třídy pro evidenci zboží.
* Použijte třídy `std::vector` a `std::string`, nepoužívejte třídu `std::map`.
* K načítání řádku ze vstupu lze použít `std::istream::getline()`. Řádek načtený ze vstupu lze potom zpracovat pomocí třídy `std::istringstream`.
* Platný vstup je zakončen odřádkováním.
* K řazení a hledání použijte funkce z knihovny `algorithm`, při vkládání využijte iterátory. (`sort`, `lower_bound`, `insert`)  