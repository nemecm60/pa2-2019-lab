#include <iostream>
#include <memory>
#include <sstream>
#include <stack>
#include <queue>

class Token {
public:
    virtual bool isEndOfInput() const {
        return false;
    }
    virtual void invoke(std::queue<std::shared_ptr<Token>>&, std::stack<std::shared_ptr<Token>>&) = 0;
    virtual int getPriority() {
        return 0;
    }
    virtual bool isLeftParenthesis() const {
        return false;
    }
    virtual std::ostream& print(std::ostream& os) const {
        return os;
    };
    friend std::ostream& operator<<(std::ostream& os, const Token& t) {
        return t.print(os);
    }
    struct Generator {
        virtual bool canGenerate(const std::string&) const = 0;
        virtual Token* generate(const std::string&) const = 0;
    };
};

class Operator : public Token {
    std::string op;
    bool hasHigherPriorityThan(Token* t) {
        return getPriority() < t->getPriority();
    }
public:
    explicit Operator(const std::string& op) : op(op) {}
    void invoke(std::queue<std::shared_ptr<Token>>&outputQueue, std::stack<std::shared_ptr<Token>>&operatorStack) override {
        if(outputQueue.empty()) { // infixový výraz nesmí začínat operátorem
            throw std::exception();
        }
        if(!operatorStack.empty() && hasHigherPriorityThan(operatorStack.top().get())) { // pokud má vršek zásobníku vyšší pioritu než token
            outputQueue.push(operatorStack.top());
            operatorStack.pop();
        }
        operatorStack.push(std::shared_ptr<Token>(new Operator(*this)));
    }
    int getPriority() override {
        if(op == "*" || op == "/") {
            return 2;
        }
        else if(op == "+" || op == "-") {
            return 1;
        }
        return 0;
    }
    std::ostream& print(std::ostream& os) const override {
        return os << op << " ";
    };
    struct Generator : public Token::Generator {
        bool canGenerate(const std::string& str) const override {
            return (str == "+" || str == "-" || str == "*" || str == "/");
        }
        Token* generate(const std::string& str) const override {
            return new Operator(str);
        }
    };
};



class Number : public Token {
    std::string number;
public:
    explicit Number(const std::string& number) : number(number) {}
    void invoke(std::queue<std::shared_ptr<Token>>&outputQueue, std::stack<std::shared_ptr<Token>>&) override {
        outputQueue.push(std::shared_ptr<Token>(new Number(*this)));
    }
    std::ostream& print(std::ostream& os) const override {
        return os << number << " ";
    };
    struct Generator : public Token::Generator {
        bool canGenerate(const std::string& str) const override {
            for(auto c : str) {
                if(c < '0' || c > '9') return false;
            }
            return true;
        }
        Token* generate(const std::string& str) const override {
            return new Number(str);
        }
    };
};

class LeftParenthesis : public Token {
public:
    void invoke(std::queue<std::shared_ptr<Token>>&, std::stack<std::shared_ptr<Token>>&operatorStack) override {
        operatorStack.push(std::shared_ptr<Token>(new LeftParenthesis));
    }
    bool isLeftParenthesis() const override {
        return true;
    }
    std::ostream& print(std::ostream&) const override {
        throw std::runtime_error("Parenthesis should not be in output");
    };
    struct Generator : public Token::Generator {
        bool canGenerate(const std::string& str) const override {
            return str == "(";
        }
        Token* generate(const std::string&) const override {
            return new LeftParenthesis();
        }
    };
};

class RightParenthesis : public Token {
public:
    void invoke(std::queue<std::shared_ptr<Token>>&outputQueue, std::stack<std::shared_ptr<Token>>&operatorStack) override {
        while(!operatorStack.empty() && !operatorStack.top()->isLeftParenthesis()) { // pošleme na výstup, co zbylo na zásobníku operátorů
            outputQueue.push(operatorStack.top());
            operatorStack.pop();
        }
        if(operatorStack.empty()) { // případ, kdy závorka ukončuje, ale není počáteční závorka
            throw std::exception();
        }
        operatorStack.pop();
    }
    std::ostream& print(std::ostream&) const override {
        throw std::runtime_error("Parenthesis should not be in output");
    };
    struct Generator : public Token::Generator {
        bool canGenerate(const std::string& str) const override {
            return str == ")";
        }
        Token* generate(const std::string&) const override {
            return new RightParenthesis();
        }
    };
};

class EndOfInput : public Token {
public:
    bool isEndOfInput() const override {
        return true;
    }
    void invoke(std::queue<std::shared_ptr<Token>>&, std::stack<std::shared_ptr<Token>>&) override {}
    struct Generator : public Token::Generator {
        bool canGenerate(const std::string& str) const override {
            return str.empty();
        }
        Token* generate(const std::string&) const override {
            return new EndOfInput();
        }
    };
};

class Parser {
    std::vector<std::shared_ptr<Token::Generator>> generators;
public:
    void addGenerator(Token::Generator* g) {
        generators.push_back(std::shared_ptr<Token::Generator>(g));
    }
    Token * parse(std::istream& iss) {
        std::string token;
        iss >> token;
        for(const auto& g : generators) {
            if(g->canGenerate(token)) {
                return g->generate(token);
            }
        }
        throw std::runtime_error("Unknown token");
    }
    std::string infixToPostfix(std::istream& iss) {
        std::queue<std::shared_ptr<Token>> outputQueue;
        std::stack<std::shared_ptr<Token>> operatorStack;
        std::shared_ptr<Token> token;
        while (!(token = std::shared_ptr<Token>(parse(iss)))->isEndOfInput()) {
            token->invoke(outputQueue, operatorStack);
        }
        while(!operatorStack.empty()) { // pošleme na výstup, co zbylo na zásobníku operátorů
            outputQueue.push(operatorStack.top());
            operatorStack.pop();
        }
        std::ostringstream oss;
        while(!outputQueue.empty()) {
            oss << *outputQueue.front() << " ";
            outputQueue.pop();
        }
        return oss.str();
    }
};

int main() {
    Parser p;
    p.addGenerator(new EndOfInput::Generator);
    p.addGenerator(new Number::Generator);
    p.addGenerator(new Operator::Generator);
    p.addGenerator(new LeftParenthesis::Generator);
    p.addGenerator(new RightParenthesis::Generator);
    {
        std::istringstream expr("3 + 3 * 5 + ( 1 + 3 ) * 6");
        std::cout << p.infixToPostfix(expr);
    }
    return 0;
}
