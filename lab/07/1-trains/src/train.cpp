#include "../header/train.h"
#include <iostream>
#include <algorithm>

Train::Train(wagon w, int count) : wagons(count, w) {}

Train::Train(int count, wagon w) : Train(w, count) {}

bool Train::empty() const {
    return wagons.empty();
}

void Train::print(wagon w) {
    std::cout << helper::wagon_str(w);
}

bool Train::operator==(const Train &t) {
    return wagons == t.wagons;
}

Train operator*(wagon w, int c) {
    return Train(w, c);
}

Train operator*(int c, wagon w) {
    return Train(w, c);
}

std::ostream &operator<<(std::ostream &o, const Train &t) {
    /*for(auto it = t.begin(); it != t.end(); ++it) { // 1. varianta
        auto w = *it;
        o << helper::wagon_str(w);
    }*/
    for(auto w : t) { // 2. varianta
        o << helper::wagon_str(w);
    }
    /*if(!t.empty()) { // puvodni verze
        o << helper::wagon_str(t.type);
        o << *t.next;
    }*/
    return o;
}

Train::operator bool() const {
    return !empty();
}

const wagon &Train::operator[](size_t i) const {
    if(i >= size()) {
        throw std::out_of_range("No wagon available.");
    }
    return wagons[i];
}

wagon &Train::operator[](size_t i) {
    if(i >= size()) {
        throw std::out_of_range("No wagon available.");
    }
    return wagons[i];
}

size_t Train::size() const {
    return wagons.size();
}

Train Train::operator+(const Train &t) const {
    return Train(*this) += t;
}

Train &Train::operator+=(const Train &t) {
    wagons.insert(wagons.end(), t.wagons.begin(), t.wagons.end());
    return *this;
}

Train Train::operator-(size_t c) const {
    return Train(*this) -= c;
}

Train &Train::operator-=(size_t c) {
    if(c >= size()) {
        wagons.clear();
    }
    else {
        wagons.erase(wagons.end() - c, wagons.end());
    }
    return *this;
}

Train operator-(size_t c, const Train &t) {
    if(c >= t.size()) {
        return Train();
    }
    return Train(t.wagons.begin() + c, t.wagons.end());
}

Train &Train::operator--() {
    return *this -= 1;
}

Train Train::operator--(int) {
    Train t(*this);
    --*this;
    return t;
}

Train::iterator Train::begin() {
    return iterator(wagons.begin());
}

Train::iterator Train::end() {
    return iterator(wagons.end());
}

Train::const_iterator Train::begin() const {
    return const_iterator(wagons.begin());
}

Train::const_iterator Train::end() const {
    return const_iterator(wagons.end());
}

Train::const_iterator Train::cbegin() const {
    return const_iterator(wagons.begin());
}

Train::const_iterator Train::cend() const {
    return const_iterator(wagons.end());
}

Train Train::reverse() {
    Train t(*this);
    std::reverse(t.wagons.begin(), t.wagons.end());
    return t;
}

Train::iterator::iterator(std::vector<wagon>::iterator it) : it(it) {}

wagon &Train::iterator::operator*() {
    return *it;
}

Train::iterator &Train::iterator::operator++() {
    ++it;
    return *this;
}

bool Train::iterator::operator!=(const Train::iterator &t) const {
    return it != t.it;
}

Train::const_iterator::const_iterator(std::vector<wagon>::const_iterator it) : it(it) {}

const wagon &Train::const_iterator::operator*() {
    return *it;
}

Train::const_iterator &Train::const_iterator::operator++() {
    ++it;
    return *this;
}

bool Train::const_iterator::operator!=(const Train::const_iterator &t) const {
    return it != t.it;
}
