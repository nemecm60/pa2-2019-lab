#pragma once

#include <iostream>

/**
 * Wagon of the train.
 */
enum struct wagon {
    LOCO = 1,
    PEOPLE,
    WOOD,
    COAL,
    END,
};

namespace helper {
    /**
     * Returns ASCII representation of wagon
     *
     * @param    w Wagon to be represented
     *
     * @return     ASCII representation of wagon
     */
    const char *wagon_str(wagon w);
}

/**
 * Sends wagon's ASCII repr. to output
 *
 * @param    os The output
 * @param     w The wagon
 *
 * @return      The output with the repr. sent to
 */
std::ostream& operator << (std::ostream& os, wagon w);
