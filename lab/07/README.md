# Náplň cvičení
Ve středu (pro zájemce):
   * OOP a návrhové vzory.
    
Ve čtvrtek bude náplň cvičení `dědičnost` a `polymorfismus`:
   * Změníme `wagon` na třídu.
   * Změníme typy `wagon` na třídy, které budou dědit ze třídy `Wagon`.
   * Doplníme funkci na zjištění údajů o pasažérech u wagonu typu `People`.  
   * Doplníme funkci na zjištění výkonu u wagonu typu `Loko`. 
   * Doplníme funkci na zjištění hmotnosti u wagonu typu `Coal` a `Woad` . 


## Dědičnost
Dědičnost slouží ke rozšiřování vlastností tříd. Základní syntaxe vypadá následovně. 

```c++
class Animal { 
    ....
};
``` 
```c++
class Dog : public Animal {
    ....
};
```

Dvojtečka zde říká, že třída Dog je založena na třídě Animal. Tato část záhlaví indikuje, že Animal je veřejná základní třída. 
Deklarujeme-li objekt Dog, bude mít tyto následující vlastnost:
   * Objekt odvozeného typu má v sobě uloženy datové členy základního typu.
   * Objekt odvozeného typu může používat metody základního typu.
   
Co je nuntno doplnit k těmto zděděným vlastnostem:
   * Odvozená třída musí mít vlastní konstruktor.
   * Odvozená třída může dle potřeby přidávat dalčí datové členy a členské funkce.

#### Konstruktor
Konstruktor musí zajišťovat data pro nové členy, pokud existují, a pro jejich zděděné členy.
 
```c++
class Animal { 
    public:
        Animal(int cnt_legs) { cnt_legs_ = cnt_legs; } 
    private:
        int cnt_legs_;
};  
``` 
```c++
class Dog : public Animal {
    public:
        Dog(const string&, int);
    private:
        string name_;
};
```
V tomto případě pak konstruktor odvozené třídy Dog může vypadat následovně. 
```c++
    Dog::Dog(const string& name, int cnt_legs) : Animal(cnt_legs) {
        name_ = name;
    }
```
Při jakékoliv implementaci konstruktoru odvozené třídy, je vždy objekt základní třídy zkonstruován první.

#### Typy dědičnosti

Existují tři typy dědičnosti:
   * public
   * protected
   * private
  
Podle způsobu dědění se rozhoduje nakolik budou v potomkovi viditelné (přístupné) metody, nebo atributy předka.
V tabulce níže je uvedeno jaký typ dědičnosti a viditělnost položky v předkovi bude mít vliv na položku v potomkovi. 
X značí, že položka nebude viditelná.

 

| dědičnost / položka v předkovi | private | protected |   public  |
|:----------------------------:|:-------:|:---------:|:---------:|
| private                      |    X    |  private  |  private  |
| protected                    |    X    | protected | protected |
| public                       |    X    | protected |   public  |

