# Dělení nosníků

Úkolem je realizovat program, který bude počítat optimální dělení ocelových nosníků.

Předpokládáme, že potřebujeme vyrobit nosníky zadaných délek. Požadované délky jsou programu předané jako seznam celých čísel. Máme k dispozici dlouhý ocelový nosník, jeho délka je rovna součtu jednotlivých požadovaných délek. Dlouhý nosník je možné rozdělit na dva menší, při dělení nedochází ke ztrátě materiálu (součet délek rozděleného nosníku se rovná délce původní). Cena za dělení je ale daná délkou děleného nosníku. Úkolem je vypočítat, jak nosník rozdělit na zadané délky za co nejnižší cenu.

Vstupem programu je seznam požadovaných délek rozdělených nosníků. Požadované délky jsou celá kladná čísla. Zadávání skončí dosažením konce vstupu (EOF). 

Výstupem programu je nalezená nejnižší cena za rozdělení nosníku na požadované délky.

Program musí kontrolovat správnost vstupních dat. Pokud je detekovaný nesprávný vstup, program zobrazí chybové hlášení a ukončí se. Za chybu je považováno:

 * prázdný seznam požadovaných délek,
* nečíselné, nulové nebo záporné délky nosníku.
## Příklady práce programu:

  * Input: `1 2 3 4 5`
  * Output: `33`
  
  
  * Input : `3 6 4 7`
  * Output: `40`
  
  
  * Input: `1 1 1 1 1 1 1 1 1 1 1 1`
  * Output: `44` 
