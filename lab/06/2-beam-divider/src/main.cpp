#include<iostream>
#include <vector>
#include <algorithm>
#include <queue>


int main() {
    std::priority_queue<int, std::vector<int>, std::greater<>> beams;
    int beam;
    while(std::cin >> beam) {
        if(beam == 0) break;
        beams.push(beam);
    }

    /*if(std::cin.bad()) {
        std::cout << "Nespravny vstup!" << std::endl;
        return 1;
    }*/

    int price = 0;

    while(beams.size() >= 2) {
        // -- totalne jsem zrusil razeni
        int beam1 = beams.top();
        beams.pop();
        int beam2 = beams.top();
        beams.pop();
        price += beam1 + beam2;
        beams.push(beam1 + beam2);
    }

    if(beams.empty()) {
        std::cout << "Nespravny vstup!" << std::endl;
        return 1;
    }

    std::cout << price << std::endl;

}

