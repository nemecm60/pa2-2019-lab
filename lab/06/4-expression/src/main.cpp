#include <iostream>
#include <memory>
#include <sstream>
#include <stack>
#include <memory>
#include <queue>

bool isOperator(const std::string& s) {
    return (s == "+" || s == "-" || s == "*" || s == "/");
}

bool isNumber(const std::string& s) {
    for(auto c : s) {
        if(c < '0' || c > '9') return false;
    }
    return true;
}

struct BinaryTree {
    std::shared_ptr<BinaryTree> left;
    std::shared_ptr<BinaryTree> right;
    char operation;
    int number;

    explicit BinaryTree(int number) : operation(0), number(number) {}
    BinaryTree(std::shared_ptr<BinaryTree>& left, char operation, std::shared_ptr<BinaryTree>& right)
        : left(left), right(right), operation(operation), number(0) {}
};

// TODO: psát závorky jen někdy
std::ostream& operator<<(std::ostream& os, const BinaryTree& bt) {
    if(bt.operation) {
        os << '(' << *bt.left << ')' << ' ' << bt.operation << ' ' << '(' << *bt.right << ')';
    }
    else {
        os << bt.number;
    }
    return os;
}

int getOperatorPriority(const std::string& a) {
    if(a == "*" || a == "/") {
        return 2;
    }
    else if(a == "+" || a == "-") {
        return 1;
    }
    return 0;
}

bool hasHigherPriority(const std::string& a, const std::string& b) {
    return getOperatorPriority(a) > getOperatorPriority(b);
}

std::shared_ptr<BinaryTree> parsePostfix(std::istream& iss) {
    std::stack<std::shared_ptr<BinaryTree>> numbers;
    std::string token;
    while(iss >> token) {
        if(isOperator(token)) {
            if(numbers.size() < 2) {
                throw std::exception();
            }
            auto b = numbers.top();
            numbers.pop();
            auto a = numbers.top();
            numbers.pop();
            if(token == "+") {
                numbers.push(std::make_shared<BinaryTree>(a, '+', b));
            }
            if(token == "-") {
                numbers.push(std::make_shared<BinaryTree>(a, '-', b));
            }
            if(token == "*") {
                numbers.push(std::make_shared<BinaryTree>(a, '*', b));
            }
            if(token == "/") {
                numbers.push(std::make_shared<BinaryTree>(a, '/', b));
            }
        }
        else if(isNumber(token)) {
            std::istringstream numberStream(token);
            int number;
            numberStream >> number;
            numbers.push(std::make_shared<BinaryTree>(number));
        }
        else {
            throw std::exception();
        }
    }
    if(numbers.size() != 1) {
        throw std::exception();
    }
    auto result = numbers.top();
    numbers.pop();
    return result;
}

std::string infixToPostfix(std::istream& iss) {
    std::string token;
    std::queue<std::string> outputQueue;
    std::stack<std::string> operatorStack;
    while (iss >> token) {
        if(isNumber(token)) {
            outputQueue.push(token);
        }
        else if(isOperator(token)) {
            if(outputQueue.empty()) { // infixový výraz nesmí začínat operátorem
                throw std::exception();
            }
            if(!operatorStack.empty() && hasHigherPriority(operatorStack.top(), token)) { // pokud má vršek zásobníku vyšší pioritu než token
                outputQueue.push(operatorStack.top());
                operatorStack.pop();
            }
            operatorStack.push(token);
        }
        else if(token == "(") {
            operatorStack.push(token);
        }
        else if(token == ")") {
            while(!operatorStack.empty() && operatorStack.top() != "(") { // pošleme na výstup, co zbylo na zásobníku operátorů
                outputQueue.push(operatorStack.top());
                operatorStack.pop();
            }
            if(operatorStack.empty()) { // případ, kdy závorka ukončuje, ale není počáteční závorka
                throw std::exception();
            }
            operatorStack.pop();
        }
        else { // neznámý symbol
            throw std::exception();
        }
    }
    while(!operatorStack.empty()) { // pošleme na výstup, co zbylo na zásobníku operátorů
        outputQueue.push(operatorStack.top());
        operatorStack.pop();
    }
    std::ostringstream oss;
    while(!outputQueue.empty()) {
        oss << outputQueue.front() << " ";
        outputQueue.pop();
    }
    return oss.str();
}

int main() {
    {
        std::istringstream expr("3 + 3 * 5 + ( 1 + 3 ) * 6");
        std::cout << infixToPostfix(expr);
    }
    return 0;


    try {
        std::istringstream expr("3 5 - 2 *");
        std::cout << *parsePostfix(expr) << std::endl;
    }
    catch(...) {
        std::cout << "Nespravny vstup." << std::endl;
    }
    return 0;
}
