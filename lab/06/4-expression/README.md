# Převody aritmetických výrazů

Napište program, který převede aritmetický výraz (+ - * / ( ) a celá čísla) z infixové notace na postfixovou a naopak.

Příklady infixové notace (nám známý běžný způsob zápisu):

```
(3 + 5) * 6
6 - 2 - 1
3 + 5 * 6
```

Příklady postfixové notace (funguje pomocí zásobníku):

```
3 5 + 6 *
6 2 - 1 -
3 5 6 * +
```

* Začneme převodem z postfixové do infixové
* Potom navážeme převodem z infixové do postfixové
* A nakonec to spojíme v jeden program, který převede cokoliv ze vstupu z jedné notace do druhé