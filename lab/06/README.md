# Náplň cvičení
Náplní cvičení bude seznámení s `STL kontejnery`.

Ve středu (pro zájemce):
* Projdeme si společně třídy queue, stack a priority_queue na různých spíše demonstračních úlohách.

Ve čtvrtek: 
* Přepíšeme náš vlak s použitím vectoru.
* Zkusíme si naimplementovat shellové utility `sort` a `unique` s použitím `vector` a `set`.


## Abstraktní datový typ (ADT)
ADT popisuje datový typ obecně bez implementačních detailů (definuje množinu hodnot a množinu operací).
Například fronta může být implementována pomocí spojového seznamenu nebo pole, ale musí mít zachovanou stejnou funkcionalitu.
 
Níže budou popsány některé abstraktní datové typy, ale již z pohledu implementace v knihovně jazyka c++. O ADT bude více v 10 a 11 přednášce.
   
 
## Vector

Vector je šablona jednorozměrného pole. 

```c++
std::vector<data_type>
```


U vectoru je zaručeno, že jsou prvky v paměti za sebou. 
Pokud je tedy do vectoru vložen prvek, na který již není kapacita, dochází k nové alokaci místa a kopírování stávajících prvků.
Pokud víte, že vector bude mít nějaký minimální počet prvků, může se k optimalizaci využít funkce
 `reserve(int capacity)` jež ve vectoru rezervuje místo o velikosti capacity.   

##### Některé členské funkce:
 * `at(size_t pos)` - vrací referenci na prvek na poszici `pos`, pokud  `!(pos < size()) `je vyhozena `std::out_of_range`.
 * `clear()` - odstraní všechny prvky z vectoru.
 * `insert(iterator pos, data_type element )` - vloží do vectoru prvek `element`, za pozici kam ukazuje iterátor `pos`.
 * `push_back(data_type element)` - přidá do vectoru prvek `element`
 * `pop_back()` - odstraní poslední prvek z vectoru.
 * `begin()` - vrátí iterátor na první prvek vectoru.
 * `end()` - vrátí iterátor na poslední prvek vectoru.
 
 Více viz https://cs.cppreference.com/w/cpp/container/vector

## Fronta (queue)
Fronta je lineární datová struktura určená pro ukládání prvků a jejich opětovný výběr ve stejném pořadí, v jakém byly do fronty přidány (`FIFO` - first in first out).
```c++
std::queue<data_type>
```

##### Některé členské funkce:
* `pop()` - odstraní první prvek ve frontě.
* `push( data_type element )` - vloží prvek `element` typu `data_type`  do fronty.
* `front()` - vrátí referenci na první prvek ve frontě.
* `back()` - vrátí referenci na poslední prvek ve frontě.

Více viz  https://en.cppreference.com/w/cpp/container/queue

## Zásobník (stack)
 Zásobník je lineární datová struktura určená pro ukládání prvků a jejich opětovný výběr v opačném pořadí, v jakém byly do fronty přidány ( `LIFO` - last in, first out). 
 
  
 ```c++
 std::stack<data_type>
 ```
 
 ##### Některé členské funkce:
 * `empty()` - vrací `true`, pokud je zásobník prázdný. Jinak `false`.
 * `size()` - vrací velikost zásobníku.
 * `push(data_type element)` - vloží prvek `element` na vrchol zásobníku.
 * `pop()` - odstraní vrchol zásobníku.
 
 Více viz  http://www.cplusplus.com/reference/stack/stack/