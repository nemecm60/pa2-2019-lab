#include <string>
#include <set>
#include <iostream>
#include <algorithm>

/**
 * Function compares two characters case insensitively
 *
 * @param a Left hand character
 * @param b Right hand character
 *
 * @return True if <a> is lesser than <b> regardless of their case
 */
bool compareCharactersCaseInsensitive(char a, char b) {
    return tolower(a) < tolower(b);
}

/**
 * Functor for comparing two strings case insensitively
 */
struct compareStringsCaseInsensitive {
    bool operator() (const std::string &a, const std::string &b) {
        return std::lexicographical_compare(a.begin(), a.end(), b.begin(), b.end(), compareCharactersCaseInsensitive);
    }
};

/**
 * Lists unique words from input.
 * Now, without differentiating letter cases.
 *
 * @return 0 = success
 */
int main() {
    std::set<std::string, compareStringsCaseInsensitive> words;

    std::string word;
    while(std::cin >> word) {
        words.insert(word);
    }

    for(const auto& w : words) {
        std::cout << w << std::endl;
    }

    return 0;
}
