#pragma once
#include <ostream>
#include <iterator>
#include <vector>

#include "wagon.h"

class Train {

    std::vector<wagon> wagons;
public:
    /**
     * Constructs empty train
     */
    Train() = default;

    /**
     * Constructs train of <count> wagons of type <wagon>
     *
     * @param w     Type of wagon
     * @param count Count of wagons
     */
    Train(wagon w, int count);

    /**
     * Constructs train of <count> wagons of type <wagon>
     *
     * @param count Count of wagons
     * @param w     Type of wagon
     */
    Train(int count, wagon w);

    /**
     * Constructs train from data given by iterators
     *
     * @tparam Iterator
     *
     * @param b Iterator to first wagon to be copied
     * @param e Iterator after last wagon to be copied
     */
    template <typename Iterator>
    Train(Iterator b, Iterator e) : wagons(b, e) {}

    class iterator : public std::iterator<std::forward_iterator_tag, wagon, std::ptrdiff_t> {
        friend Train;
        std::vector<wagon>::iterator it;
        iterator(std::vector<wagon>::iterator it);
    public:
        wagon& operator*();
        iterator& operator++();
        bool operator!=(const iterator&) const;
    };

    iterator begin();
    iterator end();

    class const_iterator : public std::iterator<std::forward_iterator_tag, const wagon, std::ptrdiff_t> {
        friend Train;
        std::vector<wagon>::const_iterator it;
        const_iterator(std::vector<wagon>::const_iterator it);
    public:
        const wagon& operator*();
        const_iterator& operator++();
        bool operator!=(const const_iterator&) const;
    };

    const_iterator begin() const;
    const_iterator end() const;

    const_iterator cbegin() const;
    const_iterator cend() const;

    bool operator==(const Train &t);

    static void print(wagon w);
    friend std::ostream& operator<<(std::ostream& o, const Train& t);

    bool empty() const;
    explicit operator bool () const;
    size_t size() const;

    const wagon& operator[](size_t i) const;

    wagon& operator[](size_t i);
    Train operator+(const Train& t) const;

    Train& operator+=(const Train& t);
    Train operator-(size_t c) const;
    Train& operator-=(size_t c);
    Train& operator--(); // <-- predekrement, protože má syntaxi unárního operátoru " -- x "

    Train operator--(int); // <-- postdekrement, protože má syntaxi binárního operátoru, " x -- 0 "

    friend Train operator-(size_t, const Train&);

    Train reverse();
};

Train operator*(wagon, int);
Train operator*(int, wagon);
