#pragma once

#include <iostream>

#include "../header/wagon.h"

const char *helper::wagon_str(wagon w) {
    switch (w) {
        case wagon::LOCO:
            return "<:^===^:>";
        case wagon::PEOPLE:
            return "[:8:::8:]";
        case wagon::WOOD:
            return ";=|===|=;";
        case wagon::COAL:
            return "[_______]";
        default:
            return "WTF?";
    }
}

std::ostream& operator << (std::ostream& os, wagon w) {
    os << helper::wagon_str(w);
    return os;
}
