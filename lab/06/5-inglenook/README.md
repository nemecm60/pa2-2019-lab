# Železniční hlavolam Inglenook sidings

Existuje celá řada posunovacích hlavolamů, jako např. jak vykřižovat dva vlaky na jedné výhybce apod.

Hlavolam Inglenook sidings představuje malé trojkolejné seřadiště, na jehož koleje se vejde 5, 3 a 3 vagony, na které navazuje výtažná kolej, na niž se vejde posunovací lokomotiva a 3 vagony. Viz obrázek.

![Schéma hlavolamu](inglenook.jpg)

_Zdroj: http://www.wymann.info/ShuntingPuzzles_

Na začátku stojí na seřadišti celkem 8 různých vagonů a na výtažné koleji je lokomotiva. Cílem je poskládat na nejdelší koleji (B) stanovenou kombinaci vagonů v co nejmenším počtu přesunů. Přesun (tah) se skládá z posunu lokomotivy (s připojenými vagony) na některou ze 3 kolejí (B, C, D), odpojení/připojení (některých) vagonů a návrat lomotivy (s připojenými vagony) na výtažnou kolej (A).

_Hlavolam je mezi vláčkaři oblíbený pro svou variablitu i pro své malé rozměry (které mu daly jméno). Na některých vláčkařských setkáních se dokonce pořádají soutěže v řešení tohoto hlavolamu. Hlavolam jde také dobře simulovat pomocí hracích karet._

Náš program má pro zadanou počáteční kombinaci vagonů (všech 8, na kolejích B a C) a cílovou kombinaci 5 vagonů (na koleji B) najít a vypsat tento nejkratší postup.

Využít můžeme faktu, že nejvyšší potřebný počet přesunů k nejrychlejšímu řešení jakéhokoliv našeho zadání je 17. [1]

Úlohu budeme řešit bez použití rekurze, a to pouze, pokud nám zbude čas ve streamu (1h minimálně).

## Pokyny k řešení

Tato úloha směřuje na prohledávání stavového prostoru a hledání nejkratší cesty v grafu (který je tvořen stavovým prostorem).

Stavový prostor si lze představit jako množinu všech možných stavů hlavolamu (na které koleji se nachází které vagony a v jakém pořadí), mezi nimiž lze přecházet platnýmy přesuny (nelze mít na jedné koleji více vagonu než dovoluje její kapacita, nelze přesouvat vagony od prostředka, nelze přesouvat vagony přímo mezi kolejemi B, C, D apod.). Tyto stavy a platné přesuny tvoří tzv. graf, čímž se více zabývají předměty BI-AG1 a BI-AG2.

Prvním úkolem tedy bude napsat reprezentaci stavů a provádění přesunů.

Dále bude potřeba napsat prohledávání stavového prostoru. K tomu lze využít algoritmus, kdy si budu udržovat nějaký "kontejner" stavů k prohledání. Na začátku do něj vložím výchozí stav. Dokud není "kontejner" prázdný, vezmu si z něj stav, který mi nabízí. Pomocí platných přesunů vytvořím nové stavy, a ty odložím zpátky do "kontejneru" k pozdějšímu zpracování. Toto provádím opakovaně. Pokud jsem si z "kontejneru" vytáhl cílový stav, úlohu jsem vyřešil. Pokud v "kontejneru" nic není, úlohu jsem nevyřešil. 

Volbou "kontejneru" lze ovlivnit, v jakém pořadí se stavový prostor prohledává:

* zásobník - depth-first search, prohledávání sleduje jednu určitou cestu, po jejím projití zkouší další
* fronta - breadth-first search, prohledávání postupně rozšiřuje svůj okruh hledání z jednoho stavu
* prioritní fronta - prohledávání probíhá s pomocí nějaké heuristiky

Kromě samotných stavů bude potřeba také hlídat počet přesunů (abychom nepřekročili maximum), a nějak ukládat jednotlivé pohyby, jimiž se k danému stavu došlo, abychom mohli vypsat řešení. 

## Další zdroje:

1. Blackburn, Simon R., Inglenook shunting puzzles, https://arxiv.org/pdf/1810.07970.pdf
