# Proktest

Číňani se rozhodli zkopírovat jistý nejmenovaný školní systém. Jenže protože jim to moc nejde, nechali to na nás... 

Úkolem je realizovat program, který bude přijímat usernamy studentů. Pokud bude zadán prázdný username, příjem skončí. Uvedeným studentům bude hodnotit jejich zdroják (simulováno náhodnou dobou trvání hodnocení a náhodným výsledkem hodnocení) v tom pořadí, v jakém přijal usernamy.

Program skončí v okamžiku, kdy je ukončen příjem a jsou ohodnoceni všichni studenti.

## Upozornění

* Program bude předveden na streamu jako demonstrace použití fronty. *Choking hazard:* Některé koncepty v ukázce budou mimo rozsah PA2 (ale mohou se některým studentům hodit v semestrální práci).

* Jiné využití fronty je v jiných příkladech.

* Další využití fronty viz nedělní stream.

* _Jakákoliv podobnost s jakýmkoliv existujícím systémem je čistě náhodná. Vyjma algoritmu hodnocení._
