#include <queue>
#include <string>
#include <iostream>
#include <random>
#include <thread>
#include <chrono>
#include <mutex>

std::mutex queueMutex;

void readStudents(std::queue<std::string>& students) {
    std::string studentName;

    while(std::cin >> studentName) {
        std::unique_lock<std::mutex> queueLock(queueMutex, std::defer_lock);
        queueLock.lock();

        students.push(studentName);
    }

    students.push("");
}

void gradeStudents(std::queue<std::string>& students) {

    std::random_device rd; // "time()"
    std::mt19937 g(rd()); // "srand()"
    std::uniform_int_distribution<int> dist(0, 500); // jaká čísla chci generovat

    while(true) {
        std::unique_lock<std::mutex> queueLock(queueMutex, std::defer_lock);
        queueLock.lock();

        if(students.empty()) {
            queueLock.unlock();
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
            continue;
        }

        std::string studentName;
        studentName = students.front();
        students.pop();

        queueLock.unlock();

        if(studentName.empty()) {
            break;
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(dist(g) * 5));
        std::cout << "Student: " << studentName << ", hodnoceni: " << (dist(g) / 100.0) << std::endl;

    }

    std::cout << "Hotovo! Pulku jsem jich vyhazel." << std::endl;

}

int main() {
    std::queue<std::string> students;

    std::thread readStudentsThread(readStudents, std::ref(students));
    std::thread gradeStudentsThread(gradeStudents, std::ref(students));

    readStudentsThread.join();
    gradeStudentsThread.join();

    return 0;
}
