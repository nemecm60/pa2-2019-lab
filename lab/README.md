# Zadání společného cvičení (ukázek na projektor)

V této složce budeme postupně a společně na jednotlivých cvičeních vypracovávat ukázkový projekt, na kterém si vyzkoušíme koncepty probírané na přednáškách.

## Téma ukázek

Rozhodli jsme se spojit ukázky jedním tématem. Není jisté, zda a jak se to povede, materiály připravujeme za pochodu, ale budeme se snažit držet se nějakého tématu.

V letošních PA1 jsem já (Honza) zadával na některých cvičeních úlohy, kde se řešení vyplňovalo do připravených funkcí. Byl to zajímavý koncept. Jedním z témat těchto úloh byly...
 
**Vlaky**

Rozhodli jsme se toto téma vzít a použít v PA2. Naše vlaky však začnou někde úplně jinde, a co z nich bude, zatím vůbec netušíme. Spořič obrazovky? Jízdní řád? Logický hlavolam? Simulátor kolejiště? Remake české "hadovky" Vlak? Nevíme. Může to být cokoliv.

Pojďte s námi, a pomozte nám tato neprobádaná místa objevit.