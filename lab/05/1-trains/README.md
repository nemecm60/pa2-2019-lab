# Další úpravy objektů

Do našeho zdrojáku `train.cpp` a do `train.h` přidáme:

Ve středu:
* Kopírovací konstruktor, operátor přiřazení. 
    * Funkce swap - prohození obsahu dvou vlaků.
    * Pravidlo tří. 
    
* Operátor porovnání - porovnání vlaků vagon po vagonu.
* Operátor násobení - výnásobení vagonu číslem.
    * vagon*3
    * 3*vagon
    
* Nahrazení funkce `print()`  přetížením operátoru `<<`.

* Operátor `[]` - vrácení reference na n-tý vagon.

* Konverze na bool - vrátí false, pokud je vlak prázdný.

Ve čtvrtek budeme pokračovat:

* Operátor sčítání - připojení jednoho vlaku za druhý.
    * Train train = train1 + train2;
    *  train += train1;

* Operátor odčítání - odečítání `x`  vagonů.
    * train1 - `x`  - odebrání posledních `x` vagonů 
    * train -= `x`  - odebrání posledních `x` vagonů
    * `x` - train   - odebrání prvních `x` vagonů

* Operátor pre i post inkrementu  - odebere poslední vagon.

* Úprava `<<` na nerekurzivní funkci.
    * Jaké řešení je efektivnější?
    
## Kopírovací konstruktor

Kopírovací konstruktor používáme ke kopírování objektu do nově vytvořeného objektu, jež probíhá v době inicializace. 

Kopírovací konstruktor má většinou tento tvar:

```c++
Class_name(const Class_name & );
```

Kopírovací konstruktor je vyvolán při každém vytvoření nového objektu a jeho inicializace na existující objekt stejného druhu.

Nechť obj je objektem typu `Class_name`, pak následující čtyři definující deklarace vyvolají kopírovací konstruktor: 
```c++

Class_name obj2(obj1); // volá Class_name(const Class_name & )
Class_name obj3 = obj1; // volá Class_name(const Class_name & )                       
Class_name obj4 = Class_name(obj1); // volá Class_name(const Class_name & )
Class_name* obj5 = new Class_name(obj1);  // volá Class_name(const Class_name & )
```

Standartní kopírovací konstruktor provádí kopírování všech nestatických členů jednoho po druhém. 
Každý člen je zkopírován hodnoutou. 

## Operátor přiřazení

Podobně jako kopírovací konstruktor, také implicitní implementace operátoru přiřazní provádí kopírovaní po složkách. 
Jestliže je složka sama objektem nějaké třídy, program provede kopírování této konkrétní položky pomocí operátoru přiřazení definovaného pro tuto třídu 
Statické položky nejsou ovlivněny

Operátor přiřazení vypadá například takto:

```c++
    class_name & operator=(const class_name & );
```
 
## Přetížení operátorů

V předchozích cvičení jsme si ukazovali přetěžování funkcí. 
Přetěžování operátorů je velice podobné konceptu přetěžování funkcí. 
V praxi to znamená, že můžeme operátorům přiřadit více významů.

V C++ je již mnoho operátorů přetíženo. Například operátor `*`. 
Můžete ho použít pro vynásobení dvou čísel nebo pro získání hodnoty na konktrétní adrese. 


Vděčným příkladem na přetížení operátorů je vector. Máme třídu:

```c++
    Vector {
    
        public:
            ...
        Vector(double, double);     
        Vector& operator+(const Vector&);
            ...
        private:
            double x; // horizontální hodnota
            double y; // vertikální hodnota
            double mag; // délka vektoru
        
    };
```
Přetížení operátoru `+` bude pak implementováno:
```c++
    Vector& Vector::operator+(const Vector& b) {
        return Vector(x + x.b, y+ y.b)
   }
    
```
