#include "../header/train.h"
#include "../header/helper.h"
#include <iostream>

void Train::print(wagon w) {
    std::cout << helper::wagon_str(w);
}

void Train::print() {
    if(!empty()) {
        print(type);
        next->print();
    }
    else {
        std::cout << std::endl;
    }
}

Train::Train() {
    type = wagon::END;
    next = nullptr;
}

Train::Train(wagon w, int count) : Train() {
    if(count > 0) {
        type = w;
        next = new Train(w, count - 1);
    }
}

Train::Train(int count, wagon w) : Train(w, count) {}

Train::~Train() {
    delete next;
}

bool Train::empty() {
    return type == wagon::END;
}