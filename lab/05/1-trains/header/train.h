
#pragma once

enum struct wagon {
    LOCO = 1,
    PEOPLE,
    WOOD,
    COAL,
    END,
};

class Train {

    wagon type;
    Train * next;
public:
    Train();
    Train(wagon w, int count);
    Train(int count, wagon w);
    ~Train();

    static void print(wagon w);
    void print();
    void removeWagon(int count = 1);
    bool empty();

};
