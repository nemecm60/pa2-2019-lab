
#pragma once

namespace helper {
    inline const char *wagon_str(wagon w) {
        switch (w) {
            case wagon::LOCO:
                return "<:^===^:>";
            case wagon::PEOPLE:
                return "[:8:::8:]";
            case wagon::WOOD:
                return ";=|===|=;";
            case wagon::COAL:
                return "[_______]";
            default:
                return "WTF?";
        }
    }
}

