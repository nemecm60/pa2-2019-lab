
#include "../header/train.h"
#include <cassert>

int main()   {
    Train train1(32, wagon::COAL) ;
    Train train2(32,  wagon::PEOPLE);
    assert(! (train1 == train2));

    train1 = Train(32, wagon::PEOPLE);
    assert(train1 == train2);

    train1 = Train(31, wagon::PEOPLE);
    assert(! (train1 == train2));
}
