
#include "../header/train.h"
#include <cassert>
#include <iostream>
#include <vector>

int main()   {
    Train train(12, wagon::COAL);
    train[11] = wagon::PEOPLE;
    assert(train[11] == wagon::PEOPLE);

    try {
        train[12] = wagon::WOOD;
    } catch (const std::out_of_range& e) {
        std::cout << "Train out of range " << e.what() << std::endl;
    }

    try {
        train[-2] = wagon::WOOD;
    } catch (const std::out_of_range& e) {
        std::cout << "Train out of range " << e.what() << std::endl;
    }

}
