
add_executable(lab-05-1-trains-test-create ../src/train.cpp create_test.cpp)
add_executable(lab-05-1-trains-test-addition ../src/train.cpp op_addition.cpp)
add_executable(lab-05-1-trains-test-comparison ../src/train.cpp op_comparison.cpp)
add_executable(lab-05-1-trains-test-decrement ../src/train.cpp op_decrement.cpp)
add_executable(lab-05-1-trains-test-multiplicity ../src/train.cpp op_multiplicity.cpp)
add_executable(lab-05-1-trains-test-subtraction ../src/train.cpp op_subtraction.cpp)
add_executable(lab-05-1-trains-test-at ../src/train.cpp op_at.cpp)