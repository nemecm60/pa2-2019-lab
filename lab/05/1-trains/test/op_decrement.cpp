
#include "../header/train.h"
#include <cassert>

int main()   {
    Train train(12, wagon::COAL);
    train[11] = wagon::PEOPLE;

    Train train1(11, wagon::COAL);
    train--;

    assert(train ==  train1);


}
