#include "../header/train.h"
#include <cassert>


int main() {
    Train train(32, wagon::COAL) ;
    train[30] = wagon::PEOPLE;
    train[31] = wagon::WOOD;

    Train train1(30, wagon::COAL);
    Train train2(30, wagon::PEOPLE);

    Train new_train = train - 2;

    assert(new_train ==  train1);
    assert(!(new_train ==  train2));

    assert(!(train ==  train1));
    assert(!(train ==  train2));

    train[27] = wagon::PEOPLE;
    train[28] = wagon::PEOPLE;
    train[29] = wagon::WOOD;

    new_train -= 3;
    assert(new_train == Train(25, wagon::COAL));


    train[0] = wagon::PEOPLE;
    train[1] = wagon::WOOD;
    new_train = 2 - train;

    assert(new_train == Train(23, wagon::COAL));


}