
#include "../header/train.h"
#include <cassert>

int main() {

    Train train1(12, wagon::PEOPLE);
    Train train2(22, wagon::PEOPLE);
    Train train = train1 + train2;

    assert(train == Train(34, wagon::PEOPLE));

}

