#define MIN(x,y) ((x)<(y)?(x):(y))
#define ANSWER_TO_LIFE_UNIVERSE_AND_EVERYTHING MIN(6*9,42)

#include "../header/train.h"


int main() {
    Train t1(wagon::LOCO, 3);
    t1.print();
    while(!t1.empty()) {
        t1.removeWagon();
        t1.print();
    }

    Train t2(ANSWER_TO_LIFE_UNIVERSE_AND_EVERYTHING, wagon::COAL);
    t1.print();
    while(!t2.empty()) {
        t2.removeWagon(10);
        t2.print();
    }

    Train t3;
    t3.print();
}