# Od funkcí k metodám

Dnes navážeme na minulé cvičení. Doprobereme datové proudy, a potom si společně procvičíme základní práci se strukturami a metodami.

Tentokrát není nachystán žádný doplňkový výklad, vše vychází z přednášek a dalších materiálů na courses.

## Společně řešené úlohy

* Přepíšeme `printf` na streamy (nestihli jsme na minulém cvičení, probereme je tedy dnes).
* Prodiskutujeme přechod na metody a s ním související problém prázdných vlaků. 
* Vytvoříme konstruktor a destruktor pro strukturu `train`.
  * Konstruktory a destruktory se berou více v pozdějších týdnech, ani my je teď nebudeme cvičit podrobně.
* Reimplementujeme stávající funkce (u kterých to lze) uvnitř struktury `train`, čímž z nich uděláme metody. 
* Přesuneme těla metod mimo strukturu `train`.
  * Jde o menší přípravu na další cvičení, kde začneme program členit do více souborů.
* Povedeme diskusi o konstantách a referencích. Při té rozdáme spoustu bodů (snad).

## Bonusy (pokud je stihneme)

* Vytvoříme funkci, která načte vlak ze souboru.
  * Nemohli bychom z ní udělat metodu?
* Vytvoříme metodu, která připojí jiný vlak za náš.
* Vytvoříme metodu, která vrátí typ n-tého vagonu, a umožní jej změnit.
