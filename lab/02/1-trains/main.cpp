#include <cstdio>

#define MIN(x,y) ((x)<(y)?(x):(y))
#define ANSWER_TO_LIFE_UNIVERSE_AND_EVERYTHING MIN(6*9,42)

enum struct wagon {
    LOCO = 1,
    PEOPLE,
    WOOD,
    COAL,
};

struct train {
    wagon type;
    train * next;
};

namespace helper {
    inline const char *wagon_str(wagon w) {
        switch (w) {
            case wagon::LOCO:
                return "<:^===^:>";
            case wagon::PEOPLE:
                return "[:8:::8:]";
            case wagon::WOOD:
                return ";=|===|=;";
            case wagon::COAL:
                return "[_______]";
            default:
                return "WTF?";
        }
    }
}

void print(wagon w) {
    printf("%s", helper::wagon_str(w));
}

void print(train * t) {
    if(t) {
        print(t->type);
        print(t->next);
    }
    else {
        printf("\n");
    }
}

train * createTrain(wagon w, int count) {
    if(count < 1) return nullptr;
    auto t = new train;
    t->type = w;
    t->next = createTrain(w, count - 1);
    return t;
}

train * createTrain(int count, wagon w) {
    return createTrain(w, count);
}

void freeTrain(train * t) {
    if(t) {
        freeTrain(t->next);
        delete t;
    }
}

void removeWagon(train *& t, int count = 1) {
    if(count > 0 && t) {
        auto tmp = t;
        t = t->next;
        delete tmp;
        removeWagon(t, count - 1);
    }
}

bool isTrainEmpty(train * t) {
    return !t;
}

int main() {
    auto t1 = createTrain(wagon::LOCO, 3);
    print(t1);
    while(!isTrainEmpty(t1)) {
        removeWagon(t1);
        print(t1);
    }
    freeTrain(t1);

    auto t2 = createTrain(ANSWER_TO_LIFE_UNIVERSE_AND_EVERYTHING, wagon::COAL);
    print(t2);
    while(!isTrainEmpty(t2)) {
        removeWagon(t2, 10);
        print(t2);
    }
    freeTrain(t2);

    print(nullptr);
}
