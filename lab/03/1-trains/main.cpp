#include <iostream>

#define MIN(x,y) ((x)<(y)?(x):(y))
#define ANSWER_TO_LIFE_UNIVERSE_AND_EVERYTHING MIN(6*9,42)

enum struct wagon {
    LOCO = 1,
    PEOPLE,
    WOOD,
    COAL,
    END,
};

struct train {
    wagon type;
    train * next;

    train(wagon w, int count);
    ~train();

    void print(wagon w);
    void print();
    void removeWagon(int count = 1);
    bool empty();
};

namespace helper {
    inline const char *wagon_str(wagon w) {
        switch (w) {
            case wagon::LOCO:
                return "<:^===^:>";
            case wagon::PEOPLE:
                return "[:8:::8:]";
            case wagon::WOOD:
                return ";=|===|=;";
            case wagon::COAL:
                return "[_______]";
            default:
                return "WTF?";
        }
    }
}

void train::print(wagon w) {
    std::cout << helper::wagon_str(w);
}

void train::print() {
    if(!empty()) {
        print(type);
        next->print();
    }
    else {
        std::cout << std::endl;
    }
}

train::train(wagon w, int count) {
    if(count < 1) {
        type = wagon::END;
        next = nullptr;
    }
    else {
        type = w;
        next = new train(w, count - 1);
    }
}

train::~train() {
    delete next;
}

void train::removeWagon(int count) {
    if(count > 0 && !empty()) {
        auto tmp = next;
        next = tmp->next;
        type = tmp->type;
        tmp->next = nullptr;
        delete tmp;
        removeWagon(count - 1);
    }
}

bool train::empty() {
    return type == wagon::END;
}

int main() {
    train * t1 = new train(wagon::LOCO, 3);
    t1->print();
    while(!t1->empty()) {
        t1->removeWagon();
        t1->print();
    }
    delete t1;

    //train * t2 = new train(ANSWER_TO_LIFE_UNIVERSE_AND_EVERYTHING, wagon::COAL);
    train * t2 = new train(wagon::COAL, ANSWER_TO_LIFE_UNIVERSE_AND_EVERYTHING);
    t1->print();
    while(!t2->empty()) {
        t2->removeWagon(10);
        t2->print();
    }
    delete t2;

    //train * t3 = new train;
    train * t3 = new train(wagon::END, 0);
    t3->print();
    delete t3;
}
