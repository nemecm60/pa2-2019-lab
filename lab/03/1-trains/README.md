# Další úpravy objektů

Upravíme náš zdroják z minulého týdne:

* Opravíme konstruktory
* Minimalizujeme opakování kódu
* Tam, kde je to vhodné, použijeme statickou metodu
* Zbavíme se zbytečných pointerů
* Rozčleníme zdroják do více souborů

## Dědění/řetězení konstruktorů

Konstruktor může volat jiný konstruktor. Lze takto zavolat jiný svůj konstruktor...

```c++
class C {
    int val;
public:
    C(int v) { val = v; }
    C() : C(42) {
        // : C(42) nejprve zavolá konstruktor C(int v), kde v = 42
        // ve složených závorkách lze potom pokračovat dalšími příkazy
    }
};
```

A stejně tak lze volat konstruktory členských proměnných:

```c++
class C2 {
    int val;
    int other;
public:
    C2(int v) : val(v), other(0) {}
};
```

Samozřejmě je možné volat svůj konstruktor i konstruktory členských promměnných. Dokonce lze volat konstruktor rodičovské třídy, což se také využívá, a budeme si ukazovat později v semestru.

Někdy to ani jinak nelze. Pokud je některá ze členských proměnných referenčního typu, je třeba ji při konstrukci inicializovat; a nelze to udělat pomocí operátoru přiřazení (=) v těle konstruktoru!

```c++
class Z {
    std::string& o;
public:
    Z(std::string& x) : o(x) {}
    // Z(std::string& x) { o = x; } <- toto nepůjde zkompilovat
};
```

## Statické metody

Pokud metoda nepracuje s instancí třídy, je možné ji deklarovat jako statickou pomocí klíčového slova `static`.

```c++
class A {
public:
    static int func();
};

int A::func() { // všimněte si, že static je jen jednou, v deklaraci
    return 42;
}
```

K volání statické metody potom není vůbec potřeba vytvářet instanci třídy.

```c++
int main() {
    return A::func();
}
```

Omezením statických metod je, že nemohou pracovat s členskými proměnnými instance, neexistuje žádné `this` (protože statické metody prostě s žádnou instancí nepracují). I členské proměnné je však možné definovat jako statické; pak se chovají úplně stejně jako globální proměnné. Statickým členům se však nyní příliš věnovat nebudeme.

## RAII

Resource acquisition is initialization.

Konstruovat objekty můžeme dynamicky - v takovém případě používáme operátor new a pracujeme s pointery. Problém je, že se o takový pointer musíme starat a ve vhodné chvíli zavolat delete. Jinak se neuvolní paměť a máme memory leak.

V jiných případech máme objekt, který pracuje s nějakým zdrojem, a po skončení práce s tímto objektem je potřeba uvolnit zdroj, uložit soubor apod. I zde bychom se měli o tuto činnost starat.

Hlídat však všechna místa, kde je potřeba se starat o uvolnění nějakého zdroje, je však obtížné, nepřehledné a náchylné k chybám.

* Pokud máme více zdrojů, je potřeba je všechny uvolňovat před každým příkazem `return`.
* Pokud dojde vyhození výjimky, musíme rovněž uvolnit zdroje. Někde, nějak. To znamená výjimky chytat, řešit, které zdroje je potřeba uvolnit (tj. kdy výjimka vznikla) atd.

Naštěstí C++ má destruktory. Jakmile nějaké hodnotě skončí platnost (typicky ukončením bloku, ve kterém vznikla), zavolá se její destruktor. Toto neplatí pro hodnoty vrácené příkazem `return` nebo vyhozené příkazem `throw`, jejichž platnost se prodlužuje do volajícího (resp. zachytávajícího) bloku.

Příklad:

```c++
int main() {
    int a = 42;
    int * aPtr = &a; 
    return 0;
}
```

Skončením uvedené funkce se zavolá destruktor typu `int` pro proměnnou `a`, a destruktor typu `int *` pro proměnnou aPtr. Protože jde o primitivní datové typy, jejich destruktory nic nedělají.

Jiný příklad:

```c++
int main() {
    int * aPtr;
    {
        int a = 42;
        aPtr = &a;
    } // (1)
    return *aPtr; // (2) 
}
```

V bodě 1 končí platnost proměnné `a`, zavolá se tedy její destruktor. V bodě 2 se odchází z funkce `main`, končí tedy platnost všech lokálních proměnných ve funkci `main`. V tomto konkrétním případě se tedy volá destruktor pro `aPtr`. Jediné, co se nedestruuje, je hodnota, která se vrací v operaci `return`. Kdyby se vracela hodnota nějaké lokální proměnné, kompilátor zařídí její kopii (tu však může optimalizovat). (Mimochodem, všimněte si, že tato ukázka nepracuje s pamětí správně.)

_Zkuste si schválně napsat třídu, jejíž konstruktor i destruktor něco vypisují, a hrajte si s jejími instancemi podle výše uvedených ukázek._

Pokud si držíme instanci jako dynamicky alokovanou (pomocí new), musíme se sami starat o její uvolnění. Pokud ale budeme držet instanci přímo, o uvolnění se postará kompilátor automaticky. Když si napíšeme vlastní destruktor, ten nám může uvolnit různé zdroje (třeba nějakou dynamickou paměť drženou ve struktuře).

Doporučením je tedy: *Snažte se minimalizovat používání pointerů a operátorů new/delete mimo třídy. Izolujte práci s pamětí a zdroji do co nejmenšího množství míst.*

Popsanému konceptu, kdy nám kompilátor zařídí automaticky uvolnění zdrojů, se říká RAII.

Díky němu:

* Lze veškerou práci s pamětí / se zdroji se přesouvá do izolovaných míst uvnitř kódu třídy.
* Lze se vyhnout chybám v práci s pamětí a jinými zdroji.
* Lze psát přehlednější kód.

Porovnejte:

```c++
int main() {
    std::ofstream * out = new std::ofstream("soubor.txt");
    *out << "Matoušek si koleduje..." << std::endl;
    delete out; // zkuste tento řádek vynechat
    return 0; 
}
```

```c++
int main() {
    std::ofstream out("soubor.txt");
    out << "Matoušek je božíííí!" << std::endl;
    return 0; 
}
```

V druhé ukázce chybí volání `out.close()` nebo jakékoliv jiná dokončovací operace. Toto však není potřeba, neboť o dokončení zápisu a uzavření souboru se postará destruktor proměnné `out`. V první ukázce jsem se o uzavření souboru musel postarat ručně (operací `delete`), protože destruktor pointeru nedělá nic.

Samozřejmě zdaleka to není všechno. Pokud třída drží nějaké zdroje, přicházejí další starosti v případě kopírování apod. To budeme řešit ve 4. a 5. týdnu.

## Fluent interface

Velice často se objevují objekty, jejichž metody vrací referenci na daný objekt.

Typickým příkladem je:

```c++
std::istream& std::istream::get(char& c) {
    // ...
    return *this;
}
```

Uvedená metoda načte znak ze vstupu, zapíše jej do reference `c`, a vrátí referenci na tentýž vstup.

Výhodou tohoto návrhu je, že lze řetězit volání metod za sebe:

```c++
int main() {
    std::ifstream in("soubor.txt");
    char a,b,c,d;
    in.get(a).get(b).get(c).get(d);
    return 0;
}
```

(Uvedený příklad není úplně nejlepší, ale existují jiné.)

Toto zjednodušuje programátorovi používání daného objektu. Není potřeba opakovaně psát název proměnné, a dokonce je možné takový sled volání metod provádět na objektu, který vznikl pouze dočasně v rámci nějakého výrazu:

```c++
class C4 {
    bool active;
    int blastRadius;
    public:
        C4() : active(false), blastRadius(5) {}
        C4& activate() {
            active = true;
            return *this;
        }
        C4& enhance() {
            blastRadius *= 2;
            return *this;        
        }
        int detonate() {
            return active ? blastRadius : 0;
        }   
};

int main() {
    return C4().enhance().enhance().activate().detonate();
}
```

Pokud tedy Vaše metoda nic nevrací, zvažte, zda by nemohla aspoň vracet referenci na svůj objekt.