#include "jmtest.h"
#include "main.h"

#include <iostream>
#include <sstream>

struct testBase64 : public jmtest {
    testBase64() : jmtest("Knihovna pro převod souborů", false) {}

    struct testIbitstream : public jmtest {

        struct testOctets : public jmtest {
            testOctets() : jmtest("Načítání celých bajtů", true) {}
            void run() const override {
                {
                    std::istringstream ss({40,50,60,73,126,0}, std::ios::binary);
                    ibitstream ibs(ss);
                    assert(ibs.get(8) == 40);
                    assert(ibs.get(8) == 50);
                    assert(ibs.get(8) == 60);
                    assert(ibs.get(8) == 73);
                    assert(ibs.get(8) == 126);
                }
                {
                    std::istringstream ss({-71,30,47,70,80,0}, std::ios::binary);
                    ibitstream ibs(ss);
                    assert(ibs.get(8) == 256 - 71);
                    assert(ibs.get(8) == 30);
                    assert(ibs.get(8) == 47);
                    assert(ibs.get(8) == 70);
                    assert(ibs.get(8) == 80);
                }
            }
        };

        struct testBits : public jmtest {
            testBits() : jmtest("Načítání jednotlivých bitů", true) {}
            void run() const override {
                {
                    std::istringstream ss({0x73, 0x40, -0x01, 0}, std::ios::binary);
                    ibitstream ibs(ss);
                    assert(ibs.get(1) == 0); assert(ibs.get(1) == 1); assert(ibs.get(1) == 1); assert(ibs.get(1) == 1); // 7
                    assert(ibs.get(1) == 0); assert(ibs.get(1) == 0); assert(ibs.get(1) == 1); assert(ibs.get(1) == 1); // 3
                    assert(ibs.get(1) == 0); assert(ibs.get(1) == 1); assert(ibs.get(1) == 0); assert(ibs.get(1) == 0); // 4
                    assert(ibs.get(1) == 0); assert(ibs.get(1) == 0); assert(ibs.get(1) == 0); assert(ibs.get(1) == 0); // 0
                    assert(ibs.get(1) == 1); assert(ibs.get(1) == 1); assert(ibs.get(1) == 1); assert(ibs.get(1) == 1); // f
                    assert(ibs.get(1) == 1); assert(ibs.get(1) == 1); assert(ibs.get(1) == 1); assert(ibs.get(1) == 1); // f
                }
            }
        };

        struct testContainedParts : public jmtest {
            testContainedParts() : jmtest("Načítání skupin bitů v rámci jednoho bajtu", true) {}
            void run() const override {
                {
                    std::istringstream ss({0x73, 0x40, -0x01, 0x61, 0x45, 0}, std::ios::binary);
                    ibitstream ibs(ss);
                    assert(ibs.get(4) == 7);
                    assert(ibs.get(4) == 3);
                    assert(ibs.get(3) == 2); assert(ibs.get(1) == 0); // 4
                    assert(ibs.get(1) == 0); assert(ibs.get(3) == 0); // 0
                    assert(ibs.get(2) == 3); assert(ibs.get(3) == 7); assert(ibs.get(3) == 7); // f
                    assert(ibs.get(8) == 0x61);
                    assert(ibs.get(5) == 8);
                    assert(ibs.get(3) == 5);
                }
            }
        };

        struct testSplitParts : public jmtest {
            testSplitParts() : jmtest("Načítání skupin bitů napříč sousedními bajty", true) {}
            void run() const override {
                {
                    std::istringstream ss({0x73, 0x40, -0x01, 0x61, 0x45, 0}, std::ios::binary);
                    ibitstream ibs(ss);
                    assert(ibs.get(4) == 7);
                    assert(ibs.get(8) == 0x34);
                    assert(ibs.get(8) == 0x0F);
                    assert(ibs.get(8) == 0xF6);
                    assert(ibs.get(8) == 0x14);
                    assert(ibs.get(4) == 0x5);
                }
                {
                    // 0111 0011 0100 0000 1111 1111 0110 0001 0100 0101 0000 0000
                    std::istringstream ss({0x73, 0x40, -0x01, 0x61, 0x45, 0}, std::ios::binary);
                    ibitstream ibs(ss);
                    assert(ibs.get(6) == 0x1c); // 0111 00.
                    assert(ibs.get(7) == 0x68); // .11 0100 0.
                    assert(ibs.get(2) == 0x00); // .00.
                    assert(ibs.get(5) == 0x0F); // .0 1111
                    assert(ibs.get(3) == 0x07); // 111.
                    assert(ibs.get(7) == 0x58); // .1 0110 00.
                    assert(ibs.get(4) == 0x05); // .01 01.
                }
            }
        };

        struct testCorrectReadFunction : public jmtest {
            testCorrectReadFunction() : jmtest("Použití správné čtecí funkce", true) {}
            void run() const override {
                {
                    std::istringstream ss({0x1D, 0x31, 0x20, 0x31, 0x00, 0x45, 0}, std::ios::binary);
                    ibitstream ibs(ss);
                    assert(ibs.get(8) == 0x1D);
                    assert(ibs.get(8) == 0x31);
                    assert(ibs.get(8) == 0x20);
                    assert(ibs.get(8) == 0x31);
                    assert(ibs.get(8) == 0x00);
                    assert(ibs.get(8) == 0x45);
                }
                {
                    std::istringstream ss({0x1D, 0x31, 0x20, 0x31, 0x00, 0x45, 0}, std::ios::binary);
                    ibitstream ibs(ss);
                    assert(ibs.get(4) == 0x1);
                    assert(ibs.get(8) == 0xD3);
                    assert(ibs.get(8) == 0x12);
                    assert(ibs.get(8) == 0x03);
                    assert(ibs.get(8) == 0x10);
                    assert(ibs.get(8) == 0x04);
                    assert(ibs.get(4) == 0x5);
                }
            }
        };

        struct testReadOver : public jmtest {
            testReadOver() : jmtest("Korektní výsledky při překročení konce vstupu", true) {}
            void run() const override {
                {
                    std::istringstream ss({0x45}, std::ios::binary);
                    ibitstream ibs(ss);
                    assert(ibs.get(8) == 0x45);
                    assert(ibs.get(8) == 0x00);
                }
                {
                    std::istringstream ss({0x45}, std::ios::binary);
                    ibitstream ibs(ss);
                    assert(ibs.get(4) == 0x4);
                    assert(ibs.get(8) == 0x50);
                }
                {
                    std::istringstream ss({0x45}, std::ios::binary);
                    ibitstream ibs(ss);
                    assert(ibs.get(4) == 0x4);
                    assert(ibs.get(5) == 0xa);
                }
            }
        };

        struct testEof : public jmtest {
            testEof() : jmtest("Korektní hlášení EOF a překročení konce vstupu", true) {}
            void run() const override {
                {
                    std::istringstream ss({0x45, 0}, std::ios::binary);
                    ibitstream ibs(ss);
                    assert(!ibs.eof() && !ibs.fail());
                    assert(ibs.get(8) == 0x45);
                    assert(!ibs.eof() && !ibs.fail());
                    assert(ibs.get(8) == 0x00);
                    assert(!ibs.eof() && !ibs.fail());
                    assert(ibs.get(1) == 0x00);
                    assert(ibs.eof() && !ibs.fail());
                }
                {
                    std::istringstream ss({0x45}, std::ios::binary);
                    ibitstream ibs(ss);
                    assert(!ibs.eof() && !ibs.fail());
                    assert(ibs.get(4) == 0x4);
                    assert(!ibs.eof() && !ibs.fail());
                    assert(ibs.get(8) == 0x50);
                    assert(ibs.eof() && ibs.fail());
                }
            }
        };

        struct testOtherFunction : public jmtest {
            testOtherFunction() : jmtest("Implementace funkce get() vracející referenci", true) {}
            void run() const override {
                {
                    std::istringstream ss1("hoja kikoti", std::ios::binary);
                    ibitstream ibs1(ss1);
                    std::istringstream ss2("hoja kikoti", std::ios::binary);
                    ibitstream ibs2(ss2);
                    for(int i = 0; i < 10; ++i) {
                        unsigned int c1 = 0, c2 = 0;
                        c1 = ibs1.get(7);
                        ibitstream& ibs3 = ibs2.get(7, c2);
                        assert(c1 == c2);
                        assert(&ibs3 == &ibs2);
                    }
                }
            }
        };

        struct testLonger : public jmtest {
            testLonger() : jmtest("Čtení delších úseků než 8 bitů", true) {}
            void run() const override {
                {
                    std::istringstream ss({0x1D, 0x31, 0x20, 0x31, 0x00, 0x45, 0}, std::ios::binary);
                    ibitstream ibs(ss);
                    assert(ibs.get(12) == 0x1D3);
                    assert(ibs.get(16) == 0x1203);
                    assert(ibs.get(20) == 0x10045);
                }
            }
        };

        testIbitstream() : jmtest("Načítání bitů ze streamu", true) {}
        void run() const override {
            assert(testOctets());
            assert(testBits());
            assert(testContainedParts());
            assert(testSplitParts());
            assert(testLonger());
            assert(testCorrectReadFunction());
            assert(testReadOver());
            assert(testEof());
            assert(testOtherFunction());
        }
    };

    struct testObitstream : public jmtest {
        struct testOctets : public jmtest {
            testOctets() : jmtest("Zápis celých bajtů", true) {}
            void run() const override {
                {
                    std::ostringstream ss(std::ios::binary);
                    {
                        obitstream obs(ss);
                        obs.put(8, 'a').put(8, 'b').put(8, 'c');
                    }
                    assert(ss.str() == std::string("abc"));
                }
                {
                    std::ostringstream ss(std::ios::binary);
                    {
                        obitstream obs(ss);
                        obs.put(8, 'a').put(8, '\0').put(8, 'c');
                    }
                    assert(ss.str() == std::string({'a','\0','c'}));
                }
            }
        };

        struct testBits : public jmtest {
            testBits() : jmtest("Zápis jednotlivých bitů", true) {}
            void run() const override {
                {
                    std::ostringstream ss(std::ios::binary);
                    {
                        obitstream obs(ss);
                        obs.put(1, 0).put(1, 1).put(1, 1).put(1, 1); // 7
                        obs.put(1, 0).put(1, 0).put(1, 1).put(1, 1); // 3
                        obs.put(1, 0).put(1, 1).put(1, 0).put(1, 0); // 4
                        obs.put(1, 0).put(1, 0).put(1, 0).put(1, 0); // 0
                        obs.put(1, 1).put(1, 1).put(1, 1).put(1, 1); // f
                        obs.put(1, 1).put(1, 1).put(1, 1).put(1, 1); // f
                    }
                    assert(ss.str() == std::string({0x73,0x40,-1}));
                }
            }
        };

        struct testContainedParts : public jmtest {
            testContainedParts() : jmtest("Zápis skupin bitů v rámci jednoho bajtu", true) {}
            void run() const override {
                {
                    std::ostringstream ss(std::ios::binary);
                    {
                        obitstream obs(ss);
                        obs.put(4, 7);
                        obs.put(4, 3);
                        obs.put(3, 2).put(1, 0); // 4
                        obs.put(1, 0).put(3, 0); // 0
                        obs.put(2, 3).put(3, 7).put(3, 7); // ff
                        obs.put(8, 0x61); // 61
                        obs.put(5, 8).put(3, 5); // 45
                    }
                    assert(ss.str() == std::string({0x73, 0x40, -0x01, 0x61, 0x45}));
                }
            }
        };

        struct testSplitParts : public jmtest {
            testSplitParts() : jmtest("Zápis skupin bitů napříč sousedními bajty", true) {}
            void run() const override {
                {
                    std::ostringstream ss(std::ios::binary);
                    {
                        obitstream obs(ss);
                        obs.put(4, 0x7);
                        obs.put(8, 0x34);
                        obs.put(8, 0x0f);
                        obs.put(8, 0xf6);
                        obs.put(8, 0x14);
                        obs.put(4, 0x5);
                    }
                    assert(ss.str() == std::string({0x73, 0x40, -0x01, 0x61, 0x45}));
                }
                {
                    std::ostringstream ss(std::ios::binary);
                    {
                        obitstream obs(ss);
                        obs.put(6, 0x1c); // 0111 00.
                        obs.put(7, 0x68); // .11 0100 0.
                        obs.put(2, 0x00); // .00.
                        obs.put(5, 0x0f); // .0 1111
                        obs.put(3, 0x07); // 111.
                        obs.put(7, 0x58); // .1 0110 00.
                        obs.put(2, 0x01); // .01
                    }
                    // 0111 0011, 0100 0000, 1111 1111, 0110 0001
                    assert(ss.str() == std::string({0x73, 0x40, -0x01, 0x61}));
                }
            }
        };

        struct testLonger : public jmtest {
            testLonger() : jmtest("Zápis delších úseků než 8 bitů", true) {}
            void run() const override {
                {
                    std::ostringstream ss(std::ios::binary);
                    {
                        obitstream obs(ss);
                        obs.put(12, 0x1D3);
                        obs.put(16, 0x1203);
                        obs.put(20, 0x10045);
                    }
                    assert(ss.str() == std::string({0x1D, 0x31, 0x20, 0x31, 0x00, 0x45}));
                }
            }
        };

        struct testWriteOver : public jmtest {
            testWriteOver() : jmtest("Korektní ukončení zápisu v případě neúplného posledního bajtu", true) {}
            void run() const override {
                {
                    std::ostringstream ss(std::ios::binary);
                    {
                        obitstream obs(ss);
                        obs.put(1, 1);
                    }
                    assert(ss.str() == std::string({'\x80'}));
                }
                {
                    std::ostringstream ss(std::ios::binary);
                    {
                        obitstream obs(ss);
                        obs.put(1, 0);
                    }
                    assert(ss.str() == std::string({'\0'}));
                }
                {
                    std::ostringstream ss(std::ios::binary);
                    {
                        obitstream obs(ss);
                        obs.put(4, 1);
                    }
                    assert(ss.str() == std::string({'\x10'}));
                }
                {
                    std::ostringstream ss(std::ios::binary);
                    {
                        obitstream obs(ss);
                        obs.put(4, 1);
                        obs.put(8, 1);
                    }
                    assert(ss.str() == std::string({'\x10', '\x10'}));
                }
                {
                    std::ostringstream ss(std::ios::binary);
                    {
                        obitstream obs(ss);
                        obs.put(8, 1);
                    }
                    assert(ss.str() == std::string({'\x01'}));
                }
            }
        };

        testObitstream() : jmtest("Zápis bitů do streamu", true) {}
        void run() const override {
            assert(testOctets());
            assert(testBits());
            assert(testContainedParts());
            assert(testSplitParts());
            assert(testLonger());
            assert(testWriteOver());
        }
    };

    struct testBase64Primitives : public jmtest {
        struct testToBase64 : public jmtest {
            testToBase64() : jmtest("Převod 6bitového kódu do Base64", true) {}

            void run() const override {
                for(unsigned int i = 0, j = 'A'; i < 26; ++i, ++j) {
                    assert(base64::helper::to(i) == j);
                }
                for(unsigned int i = 26, j = 'a'; i < 52; ++i, ++j) {
                    assert(base64::helper::to(i) == j);
                }
                for(unsigned int i = 52, j = '0'; i < 62; ++i, ++j) {
                    assert(base64::helper::to(i) == j);
                }
                assert(base64::helper::to(62) == '+');
                assert(base64::helper::to(63) == '/');
            }
        };

        struct testFromBase64 : public jmtest {
            testFromBase64() : jmtest("Převod do 6bitového kódu z Base64", true) {}

            void run() const override {
                for(unsigned int i = 0, j = 'A'; i < 26; ++i, ++j) {
                    assert(base64::helper::from(j) == i);
                }
                for(unsigned int i = 26, j = 'a'; i < 52; ++i, ++j) {
                    assert(base64::helper::from(j) == i);
                }
                for(unsigned int i = 52, j = '0'; i < 62; ++i, ++j) {
                    assert(base64::helper::from(j) == i);
                }
                assert(base64::helper::from('+') == 62);
                assert(base64::helper::from('/') == 63);
            }
        };

        struct testErrors : public jmtest {
            testErrors() : jmtest("Zpracování chybných vstupů", true) {}

            void run() const override {
                for(unsigned int i = 0; i < 0x100; ++i) {
                    if((i < 'A' || i > 'Z') && (i < 'a' || i > 'z') && (i < '0' || i > '9') && (i != '+') && (i != '/')) {
                        assert(base64::helper::from(i) == 0xff);
                    }
                }
            }
        };

        testBase64Primitives() : jmtest("Základní funkce pro převod mezi kódy", true) {}
        void run() const override {
            assert(testToBase64());
            assert(testFromBase64());
            assert(testErrors());
        }

    };

    struct testBase64Functions : public jmtest {
        struct testToBase64 : public jmtest {
            testToBase64() : jmtest("Převod raw vstupu do Base64", true) {}

            void run() const override {
                {
                    std::istringstream in("Ahoj lidi! Toto je test.", std::ios::binary);
                    std::ostringstream out(std::ios::binary);
                    assert(base64::to(in, out));
                    assert(out.str() == std::string("QWhvaiBsaWRpISBUb3RvIGplIHRlc3Qu"));
                }
            }
        };

        struct testToBase64Padded : public jmtest {
            testToBase64Padded() : jmtest("Převod raw vstupu do Base64, délka není dělitelná 3", true) {}

            void run() const override {
                {
                    std::istringstream in("Ahoj lidi! Toto je super.", std::ios::binary);
                    std::ostringstream out(std::ios::binary);
                    assert(base64::to(in, out));
                    assert(out.str() == std::string("QWhvaiBsaWRpISBUb3RvIGplIHN1cGVyLg=="));
                }
                {
                    std::istringstream in("Ahoj, lidi! Toto je super.", std::ios::binary);
                    std::ostringstream out(std::ios::binary);
                    assert(base64::to(in, out));
                    assert(out.str() == std::string("QWhvaiwgbGlkaSEgVG90byBqZSBzdXBlci4="));
                }
            }
        };

        struct testFromBase64 : public jmtest {
            testFromBase64() : jmtest("Převod Base64 vstupu do raw", true) {}

            void run() const override {
                {
                    std::istringstream in("QWhvaiBsaWRpISBUb3RvIGplIHRlc3Qu", std::ios::binary);
                    std::ostringstream out(std::ios::binary);
                    assert(base64::from(in, out));
                    assert(out.str() == std::string("Ahoj lidi! Toto je test."));
                }
            }
        };

        struct testFromBase64Padded : public jmtest {
            testFromBase64Padded() : jmtest("Převod Base64 vstupu do raw, výsledná délka není dělitelná 3", true) {}

            void run() const override {
                {
                    std::istringstream in("QWhvaiBsaWRpISBUb3RvIGplIHN1cGVyLg==", std::ios::binary);
                    std::ostringstream out(std::ios::binary);
                    assert(base64::from(in, out));
                    assert(out.str() == std::string("Ahoj lidi! Toto je super."));
                }
                {
                    std::istringstream in("QWhvaiwgbGlkaSEgVG90byBqZSBzdXBlci4=", std::ios::binary);
                    std::ostringstream out(std::ios::binary);
                    assert(base64::from(in, out));
                    assert(out.str() == std::string("Ahoj, lidi! Toto je super."));
                }
            }
        };

        struct testErrors : public jmtest {
            testErrors() : jmtest("Zpracování chybných vstupů - neplatné znaky", true) {}

            void run() const override {
                {
                    std::istringstream in("aA-.", std::ios::binary);
                    std::ostringstream out(std::ios::binary);
                    assert(!base64::from(in, out));
                }
                {
                    std::istringstream in("0123", std::ios::binary);
                    std::ostringstream out(std::ios::binary);
                    assert(base64::from(in, out));
                }
                {
                    std::istringstream in("<$c>", std::ios::binary);
                    std::ostringstream out(std::ios::binary);
                    assert(!base64::from(in, out));
                }
                {
                    std::istringstream in("", std::ios::binary);
                    std::ostringstream out(std::ios::binary);
                    assert(base64::from(in, out));
                    assert(out.str().empty());
                }
            }
        };

        struct testErrorsPadding : public jmtest {
            testErrorsPadding() : jmtest("Zpracování chybných vstupů - padding", true) {}

            void run() const override {
                {
                    std::istringstream in("aA", std::ios::binary);
                    std::ostringstream out(std::ios::binary);
                    assert(!base64::from(in, out));
                }
                {
                    std::istringstream in("aA=", std::ios::binary);
                    std::ostringstream out(std::ios::binary);
                    assert(!base64::from(in, out));
                }
                {
                    std::istringstream in("a==", std::ios::binary);
                    std::ostringstream out(std::ios::binary);
                    assert(!base64::from(in, out));
                }
                {
                    std::istringstream in("aA===", std::ios::binary);
                    std::ostringstream out(std::ios::binary);
                    assert(!base64::from(in, out));
                }
                {
                    std::istringstream in("====", std::ios::binary);
                    std::ostringstream out(std::ios::binary);
                    assert(!base64::from(in, out));
                }
                {
                    std::istringstream in("1234====", std::ios::binary);
                    std::ostringstream out(std::ios::binary);
                    assert(!base64::from(in, out));
                }
            }
        };

        testBase64Functions() : jmtest("Hlavní funkce pro převody vstupů/výstupů", true) {}
        void run() const override {
            assert(testToBase64());
            assert(testToBase64Padded());
            assert(testFromBase64());
            assert(testFromBase64Padded());
            assert(testErrors());
            assert(testErrorsPadding());
        }

    };

    void run() const override {
        assert(testIbitstream());
        assert(testObitstream());
        assert(testBase64Primitives());
        assert(testBase64Functions());
    }
};

int main() {
    testBase64 t;
    jmtest::setLogger(new jmtest::logger());
    if(t) {
        std::cout << "Gratuluji! Pošli mi zdrojáky main.cpp a main.h na e-mail jan.matousek@fit.cvut.cz" << std::endl;
    }
}
