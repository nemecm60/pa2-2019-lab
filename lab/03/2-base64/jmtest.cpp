#include "jmtest.h"

#include <iostream>

#ifndef WIN32
#define test_s "\033[1;32m"
#define test_f "\033[1;31m"
#define test_x "\033[0m"
#define test_tick "✓ "
#define test_fail "✗ "
#else
#define test_s ""
#define test_f ""
#define test_x ""
#define test_tick "[OK] "
#define test_fail "[FAIL] "
#endif

jmtest::jmtest(const char * name, bool testAll) : name(name), total(0), ok(0), skipped(0), testAll(testAll) {}

jmtest::operator bool() const {
    if(lgr) lgr->beginTest(*this);
    run();
    if(lgr) lgr->endTest(*this);
    return !(total - ok);
}

bool jmtest::assert(bool b) const {
    if(total - ok > 0 && !testAll) {
        ++skipped;
        return false;
    }
    ++total;
    if(b) {
        ++ok;
    }
    return b;
}

bool jmtest::assert(const jmtest& t) const {
    if(total - ok > 0 && !testAll) {
        ++skipped;
        return false;
    }
    return assert((bool)t);
}

void jmtest::setLogger(jmtest::logger *l) {
    lgr = std::shared_ptr<jmtest::logger>(l);
}

std::shared_ptr<jmtest::logger> jmtest::lgr(nullptr);

jmtest::logger::logger() : depth(0) {}

void jmtest::logger::beginTest(const jmtest &t) {
    if(!t.name) return;
    std::cout << std::string(depth * 2, ' ') << "* Testing: " << t.name << std::endl;
    ++depth;
}

void jmtest::logger::endTest(const jmtest &t) {
    if(!t.name) return;
    --depth;
    std::cout << std::string(depth * 2, ' ');
    if(!(t.total - t.ok)) {
        std::cout << test_s << test_tick;
    }
    else {
        std::cout << test_f << test_fail;
    }
    std::cout << t.name << " (" << t.ok << "/" << t.total << " + " << t.skipped << " skipped)" << test_x << std::endl;
}
