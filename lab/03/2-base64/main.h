#pragma once

#include <iosfwd>

/*
 * 1. část
 *
 * Knihovna pro práci s bitovými proudy.
 * Do definic tříd samozřejmě můžete přidat cokoliv svého.
 */

class ibitstream {
public:
    explicit ibitstream(std::istream&);
    ibitstream& get(unsigned int, unsigned int&);
    unsigned int get(unsigned int);
    bool eof();
    bool fail();
    bool bad();
};

class obitstream {
public:
    explicit obitstream(std::ostream&);
    obitstream& put(unsigned int, unsigned int);
    bool eof();
    bool fail();
    bool bad();
};

/*
 * 2. část
 *
 * Knihovna pro převod z/do Base64
 */

namespace base64 {
    namespace helper {
        unsigned int to(unsigned int);
        unsigned int from(unsigned int);
    }

    bool to(std::istream&, std::ostream&);
    bool from(std::istream&, std::ostream&);
}
