# Práce s binárními soubory

Zadáním je napsat knihovnu, která je schopna číst a zapisovat binární soubory po bitech. Tuto knihovnu potom využijeme pro převod souborů z/do Base64. Správnost Vaší implementace zkontroluje Vaše oblíbená testovací knihovna z mých cvik PA1.

## Binární soubory po bitech

Předpokládáme, že soubor je série bitů, kdy čtení začíná nejvyšším bitem daného bajtu, pokračuje nižšími bity až k nejnižšímu, po kterém navazuje nejvyšší bit dalšího bajtu. Půjde tedy, kdybychom chtěli číst/zapisovat vícebajtová čísla, o kódování big endian. Toto uspořádání je také logické pro nás, protože se dobře ukazuje.

Mějme soubor s tímto obsahem:

```
ABCD
```

To jsou ASCII znaky číslo 65, 66, 67 a 68, v binární podobě tedy:

```
01000001 01000010 01000011 01000100
```

Tento soubor lze chápat jako sérii jedniček a nul prostým navázáním binárních zápisů čísel za sebe:

```
01000001010000100100001101000100
```

C++ neobsahuje žádné knihovny pro práci s datovými proudy po bitech. Datové proudy fungují po bajtech, případně skupinách bajtů (pokud se rozhodnete pro `wstream`). Musíme si tedy takový proud napsat sami.

V knihovně se proto nacházejí třídy `ibitstream` a `obitstream`, které obalují klasický bajtový vstupní a výstupní proud. Tyto třídy potom nabízejí metody `get` a `put` zadaného počtu bitů. (Nějaké pokročilejší metody, přetížení proudových operátorů apod. řešit nebudeme.)

Takovému obalování jednoho objektu jiným za účelem poskytnutí jiného rozhraní či chování se říká _adapter_. (Objektové návrhové vzory jsou však mimo rámec předmětu PA2, proto zde nebudu uvádět detaily; navíc jsou tenké hranice mezi různými vzory, např. uvedené schéma je podobné vzoru _proxy_.)

![Objektový model obalení](model.png)

Aby bylo možné zapisovat a číst po bitech, musíme pracovat s nějakým bufferem, se kterým jsme schopni pracovat po bitech pomocí bitových operací. Teprve tehdy, když je buffer prázdný, načteme další bajt, případně pokud je plný, zapíšeme jej do proudu.

Nejjednodušší je napsat si nejprve funkci, která čte/zapisuje po jednom bitu. To je triviální operace, kdy je potřeba z bufferu vytáhnout správný bit, správným směrem posunout zásobník, a bit zapsat správným způsobem do výsledku. Následně lze načítáním jednotlivých bitů v cyklu zařídit načítání libovolného počtu bitů.

Předpokládáme, že nejdelší čtené/zapisované číslo má rozsah datového typu `int`, limity typu `int` však nebude testovací prostředí testovat, nemusíte tedy kontrolovat rozsahy.   

Je potřeba si také dát pozor na chování na konci souboru, a případně doplnit nějaké vhodné metody a dokončení práce se souborem podle zásad RAII.

## Ošetření chyb při práci s proudy

Proudy v C++ jsou hezké, ale také velice abstraktní. Pokud např. má selhat nějaká operace, proud to signalizuje až poté, co takovou operaci zkusí.

### Konec souboru?

Musíte se pokusit přečíst něco za koncem proudu, aby proud nastavil svůj eofbit.

```c++
char c;
std::istream i;
c = i.get(); // pokusíme se načíst bajt
if(i.eof()) { ... }
```
  
### Soubor nejde otevřít?

Toto naštěstí je vyřešeno při konstrukci proudu...

```c++
char c;
std::ifstream i("soubor.txt", std::ios::binary);
if(i) { ... }
```

### Soubor nejde číst?

Musíte nejprve něco přečíst.

```c++
char c;
std::ifstream i("soubor.txt", std::ios::binary);
c = i.get();
if(i) { ... }
```

### Soubor nejde zapsat?

Musíte nejprve něco zapsat.

```c++
std::ofstream o("soubor.txt", std::ios::binary);
o.put('x');
if(o) { ... }
```

### Další problémy?

Vřele doporučuji prostudovat si příslušnou příručku jazyka. Hlavně se mohou hodit metody eof(), fail(), bad(), případně také clear() a seekg().

Při testování programů pracujících se soubory nezapomeňte také vyzkoušet soubory s neplatnými daty, různě useknuté, nebo dokonce prázdné.

## Další doporučená literatura

* Obrátil, Karel Jaroslav. Velký slovník sprostých slov.
* Murphy, Edward Aloysius Jr. et al. Zákony.