#pragma once

#include <iosfwd>
#include <memory>

/*
 * Testovací knihovna, prosím neupravovat!
 */

class jmtest {
public:
    class logger;
private:
    const char * name;
    mutable int total;
    mutable int ok;
    mutable int skipped;
    bool testAll;
    static std::shared_ptr<logger> lgr;
public:
    jmtest(const char * = nullptr, bool = false);
    virtual void run() const = 0;
    explicit operator bool() const;
    static void setLogger(logger *l);
protected:
    bool assert(bool) const;
    bool assert(const jmtest&) const;
};

class jmtest::logger {
    int depth;
    friend jmtest;
public:
    logger();
    void beginTest(const jmtest&);
    void endTest(const jmtest&);
};
