#include "main.h"

#include <iostream>

/*
 * 1. část
 *
 * Knihovna pro práci s bitovými proudy
 */

/**
 * Konstruktor třídy ibitstream
 * @param in Proud, ze kterého se budou načítat bity
 */
ibitstream::ibitstream(std::istream& in) {}

/**
 * Přečte <bits> bitů ze vstupu a výsledný údaj zapíše do proměnné <data>
 * Funkce vrátí referenci na tuto instanci
 *
 * Pokud se načtená sekvence nachází celá za koncem vstupu, nastaví se eof stav
 * Pokud se načtená sekvence nachází částečně za koncem vstupu, nastaví se eof a fail stav
 * Pokud výsledek obsahuje část, která se nachází za koncem vstupu, má tato část nulové bity
 *
 * @param bits Počet bitů k načtení
 * @param data
 * @return
 */
ibitstream& ibitstream::get(unsigned int bits, unsigned int& data) {
    return *this;
}

/**
 * Přečte <bits> bitů ze vstupu a výsledný údaj vrátí
 *
 * Pokud se načtená sekvence nachází celá za koncem vstupu, nastaví se eof stav
 * Pokud se načtená sekvence nachází částečně za koncem vstupu, nastaví se eof a fail stav
 * Pokud výsledek obsahuje část, která se nachází za koncem vstupu, má tato část nulové bity
 *
 * @param bits Počet bitů k načtení
 * @return
 */
unsigned int ibitstream::get(unsigned int bits) {
    return 0;
}

/**
 * Vrátí, zda bylo dosaženo konce vstupu.
 *
 * @return
 */
bool ibitstream::eof() {
    return false;
}

/**
 * Vrátí, zda došlo k nekorektnímu čtení.
 *
 * @return
 */
bool ibitstream::fail() {
    return false;
}

/**
 * Vrátí, zda došlo k chybě při čtení.
 *
 * @return
 */
bool ibitstream::bad() {
    return false;
}

/**
 * Konstruktor třídy obitstream
 * @param out Proud, do kterého se budou zapisovat bity
 */
obitstream::obitstream(std::ostream& out) {

}

/**
 * Zapíše <bits> bitů proměnné <data> do výstupu
 * Funkce vrátí referenci na tuto instanci
 *
 * @param bits Počet bitů k zapsání
 * @param data
 * @return
 */
obitstream& obitstream::put(unsigned int bits, unsigned int data) {
    return *this;
}

/**
 * Vrátí, zda výstup skončil (toto u výstupu nemá moc smysl, a ani se to netestuje)
 *
 * @return
 */
bool obitstream::eof() {
    return false;
}

/**
 * Vrátí, zda došlo k nekorektnímu zápisu (chyba formátu apod.; opět se toto netestuje)
 *
 * @return
 */
bool obitstream::fail() {
    return false;
}

/**
 * Vrátí, zda došlo k nějaké chybě při zápisu.
 *
 * @return
 */
bool obitstream::bad() {
    return false;
}

/*
 * 2. část
 *
 * Knihovna pro převod z/do Base64
 */

/**
 * Převede 6bitové číslo do jeho Base64 znakové podoby
 * V případě chyby vrátí 0xff.
 *
 * @param raw
 * @return
 */
unsigned int base64::helper::to(unsigned int raw) {
    return 0;
}

/**
 * Převede 8bitový znak z Base64 do 6bitového čísla
 * V případě chyby vrátí 0xff.
 *
 * @param coded
 * @return
 */
unsigned int base64::helper::from(unsigned int coded) {
    return 0;
}

/**
 * Převede data ze vstupního proudu do Base64
 *
 * @param in Vstupní proud s daty v raw formátu
 * @param out Výstupní proud pro data v Base64 formátu
 * @return Zda převod uspěl
 */
bool base64::to(std::istream& in, std::ostream& out) {
    return false;
}

/**
 *
 * @param in
 * @param out
 * @return
 */
bool base64::from(std::istream& in, std::ostream& out) {
    return false;
}
