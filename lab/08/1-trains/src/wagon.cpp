#include <iostream>

#include "../header/wagon.h"

Wagon::Wagon(int l) : length(l) {}

int Wagon::getLength() {
    return length;
}

bool Wagon::isFull() {
    return false;
}

void Wagon::addPeople(const std::string &) {}

Wagon::~Wagon() {}

std::ostream &Wagon::print(std::ostream &os) const {
    return os;
}

std::ostream& operator << (std::ostream& os, const Wagon& w) {
    return w.print(os);
}

bool Wagon::operator==(const Wagon &w) const {
    return length == w.length;
}

Wagon *Wagon::clone() const {
    return nullptr;
}

Locomotive::Locomotive() : Wagon(20) {}

std::ostream &Locomotive::print(std::ostream &os) const {
    return os << *this;
}

Wagon *Locomotive::clone() const {
    return new Locomotive(*this);
}

std::ostream &operator<<(std::ostream &os, const Locomotive &) {
    return os << "<:^===^:>";
}

PeopleWagon::PeopleWagon() : Wagon(40) {}

void PeopleWagon::addPeople(const std::string &name) {
    people.push_back(name);
}

PeopleWagon::~PeopleWagon() {}

std::ostream &PeopleWagon::print(std::ostream &os) const {
    return os << *this;
}

Wagon *PeopleWagon::clone() const {
    return new PeopleWagon(*this);
}

std::ostream &operator<<(std::ostream &os, const PeopleWagon &) {
    return os << "[:8:::8:]";
}

WoodWagon::WoodWagon() : Wagon(30), full(false) {}

bool WoodWagon::isFull() {
    return full;
}

void WoodWagon::load() {
    full = true;
}

void WoodWagon::unload() {
    full = false;
}

std::ostream &WoodWagon::print(std::ostream &os) const {
    return os << *this;
}

Wagon *WoodWagon::clone() const {
    return new WoodWagon();
}

std::ostream &operator<<(std::ostream &os, const WoodWagon &) {
    return os << ";=|===|=;";
}

CoalWagon::CoalWagon() : Wagon(15) {}

std::ostream &CoalWagon::print(std::ostream &os) const {
    return os << *this;
}

Wagon *CoalWagon::clone() const {
    return new CoalWagon();
}

std::ostream &operator<<(std::ostream &os, const CoalWagon &) {
    return os << "[_______]";
}
