#include "../header/train.h"
#include <iostream>
#include <algorithm>
#include <numeric>

Train::Train(Wagon &w, int count) {
    for(int i = 0; i < count; ++i) {
        wagons.push_back(w.clone());
    }
}
Train::Train(Wagon &&w, int count) : Train(w, count) {}

Train::Train(int count, Wagon &w) : Train(w, count) {}
Train::Train(int count, Wagon &&w) : Train(w, count) {}

Train::Train(const Train& t) : Train() {
    for(auto wp : t.wagons) {
        wagons.push_back(wp->clone());
    }
}
Train::Train(Train&& t) : Train() {
    std::swap(wagons, t.wagons);
}

Train& Train::operator=(Train t) {
    std::swap(wagons, t.wagons);
    return *this;
}

Train::~Train() {
    for(auto wp : wagons) {
        delete wp;
    }
}

bool Train::empty() const {
    return wagons.empty();
}

bool Train::operator==(const Train &t) {
    return wagons == t.wagons;
}

/*
Train operator*(Wagon w, int c) {
    return Train(w, c);
}

Train operator*(int c, Wagon w) {
    return Train(w, c);
}
*/

std::ostream &operator<<(std::ostream &o, const Train &t) {
    /*for(auto it = t.begin(); it != t.end(); ++it) { // 1. varianta
        auto w = *it;
        o << helper::Wagon_str(w);
    }*/
    for(const auto& w : t) { // 2. varianta
        o << w;
    }
    /*if(!t.empty()) { // puvodni verze
        o << helper::Wagon_str(t.type);
        o << *t.next;
    }*/
    return o;
}

Train::operator bool() const {
    return !empty();
}

const Wagon &Train::operator[](size_t i) const {
    if(i >= size()) {
        throw std::out_of_range("No Wagon available.");
    }
    return *wagons[i];
}

Wagon &Train::operator[](size_t i) {
    if(i >= size()) {
        throw std::out_of_range("No Wagon available.");
    }
    return *wagons[i];
}

size_t Train::size() const {
    return wagons.size();
}

int Train::length() const {
    return std::accumulate(
            wagons.begin(), wagons.end(),
            0,
            [](int result, Wagon* wp) { return result + wp->getLength(); });
}

Train Train::operator+(const Train &t) const {
    return Train(*this) += t;
}

Train &Train::operator+=(const Train &t) {
    for(auto wp : t.wagons) {
        wagons.push_back(wp->clone());
    }
    return *this;
}

Train Train::operator-(size_t c) const {
    return Train(*this) -= c;
}

Train &Train::operator-=(size_t c) {
    if(c >= size()) {
        for(auto wp : wagons) {
            delete wp;
        }
        wagons.clear();
    }
    else {
        for(auto it = wagons.end() - c; it != wagons.end(); ++it) {
            delete *it;
        }
        wagons.erase(wagons.end() - c, wagons.end());
    }
    return *this;
}

Train operator-(size_t c, const Train &t) {
    if(c >= t.size()) {
        return Train();
    }
    return Train(t.wagons.begin() + c, t.wagons.end());
}

Train &Train::operator--() {
    return *this -= 1;
}

Train Train::operator--(int) {
    Train t(*this);
    --*this;
    return t;
}

Train::iterator Train::begin() {
    return iterator(wagons.begin());
}

Train::iterator Train::end() {
    return iterator(wagons.end());
}

Train::const_iterator Train::begin() const {
    return const_iterator(wagons.begin());
}

Train::const_iterator Train::end() const {
    return const_iterator(wagons.end());
}

Train::const_iterator Train::cbegin() const {
    return const_iterator(wagons.begin());
}

Train::const_iterator Train::cend() const {
    return const_iterator(wagons.end());
}

Train Train::reverse() {
    Train t(*this);
    std::reverse(t.wagons.begin(), t.wagons.end());
    return t;
}

Train::iterator::iterator(std::vector<Wagon*>::iterator it) : it(it) {}

Wagon &Train::iterator::operator*() {
    return **it;
}

Train::iterator &Train::iterator::operator++() {
    ++it;
    return *this;
}

bool Train::iterator::operator!=(const Train::iterator &t) const {
    return it != t.it;
}

Train::const_iterator::const_iterator(std::vector<Wagon*>::const_iterator it) : it(it) {}

const Wagon &Train::const_iterator::operator*() {
    return **it;
}

Train::const_iterator &Train::const_iterator::operator++() {
    ++it;
    return *this;
}

bool Train::const_iterator::operator!=(const Train::const_iterator &t) const {
    return it != t.it;
}
