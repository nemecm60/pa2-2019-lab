#pragma once

#include <iostream>
#include <vector>

class Wagon {
private:
    // Locomotive je zdedi jako private, ale nebudou pristupne ani pro ni
protected:
    // Locomotive je zdedi jako private nebo jako protected
    /**
     * Length in meters
     */
    int length = 50;
    explicit Wagon(int);
    virtual std::ostream& print(std::ostream& os) const;
public:
    // Locomotive je zdedi jako private, public, nebo protected
    virtual ~Wagon();
    int getLength();
    virtual bool isFull();
    virtual void addPeople(const std::string&);

    friend std::ostream& operator << (std::ostream& os, const Wagon& w);

    bool operator==(const Wagon&) const;
    virtual Wagon* clone() const;
};

class Locomotive : public Wagon {
    std::ostream& print(std::ostream& os) const override;
public:
    Locomotive();
    Wagon* clone() const override;
};

std::ostream& operator << (std::ostream& os, const Locomotive& w);

class PeopleWagon : public Wagon {
    std::vector<std::string> people;
    std::ostream& print(std::ostream& os) const override;
public:
    PeopleWagon();
    ~PeopleWagon() override;
    void addPeople(const std::string&) override;
    Wagon* clone() const override;
};

std::ostream& operator << (std::ostream& os, const PeopleWagon& w);

class WoodWagon : public Wagon {
    bool full;
    std::ostream& print(std::ostream& os) const override;
public:
    WoodWagon();
    bool isFull() override;
    void load();
    void unload();
    Wagon* clone() const override;
};

std::ostream& operator << (std::ostream& os, const WoodWagon& w);

class CoalWagon : public Wagon {
    std::ostream& print(std::ostream& os) const override;
public:
    CoalWagon();
    Wagon* clone() const override;
};

std::ostream& operator << (std::ostream& os, const CoalWagon& w);
