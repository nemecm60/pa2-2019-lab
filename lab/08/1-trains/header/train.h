#pragma once
#include <ostream>
#include <iterator>
#include <vector>

#include "wagon.h"

class Train {

    std::vector<Wagon*> wagons;
public:
    /**
     * Constructs empty train
     */
    Train() = default;

    /**
     * Constructs train of <count> wagons of type <Wagon&>
     *
     * @param w     Type of Wagon&
     * @param count Count of wagons
     */
    Train(Wagon& w, int count);
    Train(Wagon&& w, int count); // we need to get temporary Wagon objects by reference, we mustn't get copy (= lose polymorphism) or const reference (cannot store in array of non-const wagons)

    /**
     * Constructs train of <count> wagons of type <Wagon&>
     *
     * @param count Count of wagons
     * @param w     Type of Wagon&
     */
    Train(int count, Wagon& w);
    Train(int count, Wagon&& w);

    Train(const Train& t);
    Train(Train&& t);

    Train& operator=(Train t);

    ~Train();

    /**
     * Constructs train from data given by iterators
     *
     * @tparam Iterator
     *
     * @param b Iterator to first Wagon& to be copied
     * @param e Iterator after last Wagon& to be copied
     */
    template <typename Iterator>
    Train(Iterator b, Iterator e) : wagons(b, e) {}

    class iterator : public std::iterator<std::forward_iterator_tag, Wagon*, std::ptrdiff_t> {
        friend Train;
        std::vector<Wagon*>::iterator it;
        iterator(std::vector<Wagon*>::iterator it);
    public:
        Wagon& operator*();
        iterator& operator++();
        bool operator!=(const iterator&) const;
    };

    iterator begin();
    iterator end();

    class const_iterator : public std::iterator<std::forward_iterator_tag, const Wagon*, std::ptrdiff_t> {
        friend Train;
        std::vector<Wagon*>::const_iterator it;
        const_iterator(std::vector<Wagon*>::const_iterator it);
    public:
        const Wagon& operator*();
        const_iterator& operator++();
        bool operator!=(const const_iterator&) const;
    };

    const_iterator begin() const;
    const_iterator end() const;

    const_iterator cbegin() const;
    const_iterator cend() const;

    bool operator==(const Train &t);

    friend std::ostream& operator<<(std::ostream& o, const Train& t);

    bool empty() const;
    explicit operator bool () const;
    size_t size() const;

    int length() const;

    const Wagon& operator[](size_t i) const;

    Wagon& operator[](size_t i);
    Train operator+(const Train& t) const;

    Train& operator+=(const Train& t);
    Train operator-(size_t c) const;
    Train& operator-=(size_t c);
    Train& operator--(); // <-- predekrement, protože má syntaxi unárního operátoru " -- x "

    Train operator--(int); // <-- postdekrement, protože má syntaxi binárního operátoru, " x -- 0 "

    friend Train operator-(size_t, const Train&);

    Train reverse();
};

//Train operator*(Wagon&, int);
//Train operator*(int, Wagon&);
