# Náplň cvičení

Ve `čtvrtek` doděláme co jsem nestihli:
   * přetížený oprátory na porovnání vláků
   * přetížený oprátory na násobení čísla a wagonu

A dále se budeme věnovat objektovému návrhu složitějších programů a to:
   * prohlíčeč obrázků
   * simulátor/editor číslicových obvodů
   * nástroj na sazbu matematických rovnic

## Prohlížeč obrázků

Nástroj pro procházení obrázků v daném souboru. Předpokládejme, že obrázky mohou být v různých formátech.

Implementujte následující funcionalitu:
* Rotace obrázku
* Změna rozlišení obrázku
* Vytvořit z daného obrázku černobílý obrázek
* Zesvětlit/ztmavit obrázek
* Vrátit změny zpět 

Po provedení některé z operací, bude možné změněný obrázek uložit. 

## Simulátor číslicových obvodů

Program umožní umístit logická hradla (and, or, xor, klopné obvody, vstupy, výstupy, zdroj hodinového signálu) a jejich propojení vodiči.

* Program zabrání spojení výstupů.
* Program bude živě simulovat chování nakresleného obvodu.
* Program vypočítá celkovou cenu použitých součástek a vypíše BOM.

## Nástroj na sazbu matematických rovnic

Program načte matematickou rovnici z infixové notace, kde mohou být funkce, čísla, proměnné, operátory, závorky.

Poté program vygeneruje vysázenou rovnici s korektními horními indexy, zlomkovými čarami atd.

Výstup může být v HTML, ASCII nebo jako obrázek.
