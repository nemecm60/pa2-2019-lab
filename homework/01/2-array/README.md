# Nafukovací pole

Vaším úkolem je naprogramovat nafukovací pole s následujícími funkcemi:

- void init(size_t capacity) - inicializuje pole o velikosti capacity.
- void push_back(int value) -  vloží prvek na konec pole. Pokud je velikost rovna kapacitě, zvětší pole na dvojnásobek aktuální velikosti.
- void pop_back() - smaže poslední prvek pole; pokud je pole prázdné, neudělá nic.
- void reserve(size_t new_capacity) - potenciálně zvětší kapacitu vektoru na velikost new_capacity. Pokud new_capacity není větší než aktuální kapacita vektoru, nic se nezmění.
- void clear() - vyčistí pole (zachová kapacitu pole). 
- void destroy() - destruuje (uvolní) data pole a vynuluje příslušné proměnné.
- void print() - vypíše pole.

Dále doplňte klíčové slovo `const`, kam uznáte za vhodné. 

Poznámka: Používejte konstrukty jazyka C++ (`new`, `delete`, ...).

## Nápověda:

* Výše uvedené funkce se nacházejí ve jmenném prostoru struktury spolu s členskými proměnnými. Funkce takto může přistupovat ke členským proměnným konkrétní instance struktury, když je zavolána na dané instanci s pomocí tečky (případně šipky, pokud pracujeme s pointerem na strukturu), viz příklad:

```c++
#include <iostream>

struct A {
    int value;

    void print() {
        std::cout << value << std:endl;
    }
};

int main() {
    A a; // vytvoří proměnnou a, která je typu A
    B b;
    a.value = 5;
    b.value = 42;
    a.print(); // zavolaná funkce print bude mít přístup k a.value
    b.print(); // v tomto případě k b.value
}
```

* Výše uvedenému druhu funkce se říká metoda, a budou nás trápit do konce semestru.
* Může se stát, že název parametru funkce překryje název členské proměnné ve struktuře (nebo jinou proměnnou v témže jmenném prostoru). V takovém případě lze např. (kromě přejmenování parametru) využít rozlišovací operátor `::`

```c++
#include <iostream>

struct A {
    int value;

    void print(int value) {
        std::cout << value << std:endl; // vypíše parametr funkce value
        std::cout << A::value << std:endl; // vypíše hodnotu value v instanci struktury
    }
};

int main() {
    A a; // vytvoří proměnnou a, která je typu A
    A b;
    a.value = 5;
    b.value = 42;
    a.print(1); // zavolaná funkce print bude mít přístup k a.value
    b.print(2); // v tomto případě k b.value
}
```

* Další možností je použít pointer `this`, ale o tom někdy příště.
* Neopakujte Váš kód - nebojte se např. metodu reserve využít v jiných metodách.
* Šetřte procesor. Je opravdu potřeba při některých akcích procházet vlastní pole?
* Podobné nafukovací pole existuje v knihovně STL, ale hrát si s ním budeme až za několik týdnů. 
