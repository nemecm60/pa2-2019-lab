#include <iostream>
#include <cassert>


typedef struct array {
    
    int* data;
    size_t capacity;
    size_t size;


    /* inicializuje pole o velikosti (size) capacity */
    void init(size_t capacity) { /*TODO*/ }
    
    /* vloží prvek na konec pole. Pokud je velikost (size) rovna kapacitě (capacity), zvětší pole na dvojnásobek aktuální velikosti */
    void push_back(int value) { /*TODO*/ }
    
    /* smaže poslední prvek pole */
    void pop_back() { /*TODO*/ }

    /* potenciálně zvětší kapacitu vektoru na velikost new_capacity. Pokud je new_capacity rovna aktuální kapacitě vektoru, nic se nezmění */
    void reserve(size_t new_capacity) { /*TODO*/ }
    
    /* vyčistí pole */
    void clear() { /*TODO*/ }

    /* destruuje pole */
    void destroy() { /*TODO*/ } 

    /* vypíše pole */
    void print() { /*TODO*/ }

} ARRAY;

int main() {
    ARRAY array;
    array.init(10);
    assert(array.capacity ==  10  && array.size == 0 );
    array.push_back(1);
    assert(array.capacity ==  10  && array.size == 1 && array.data[0] == 1 );
    array.push_back(2);
    assert(array.capacity ==  10  && array.size == 2 && array.data[0] == 1 && array.data[1] == 2 );
    array.clear();
    assert(array.capacity ==  10  && array.size == 0 );
    array.destroy();
    assert(array.capacity ==  0  && array.size == 0 );
    
    array.init(1);
    array.push_back(1);
    array.push_back(2);
    assert(array.capacity == 2 && array.size == 2  && array.data[0] == 1 && array.data[1] == 2);
    array.push_back(3);
    assert(array.capacity == 4 && array.size == 3  && array.data[0] == 1 && array.data[1] == 2 && array.data[2] == 3);
    array.reserve(2);
    assert(array.capacity == 4 && array.size == 3  && array.data[0] == 1 && array.data[1] == 2 && array.data[2] == 3);
    array.reserve(10);
    assert(array.capacity == 10 && array.size == 3  && array.data[0] == 1 && array.data[1] == 2 && array.data[2] == 3);
    array.pop_back();
    assert(array.capacity == 10 && array.size == 2  && array.data[0] == 1 && array.data[1] == 2 );
    array.pop_back();
    assert(array.capacity == 10 && array.size == 1  && array.data[0] == 1  );
}
