# Problém batohu

Naprogramujte rozhodovací verzi problému batoh. 

Dostanete soubor, ve kterém mají jednotlivé záznamy následující formát:

| id | počet věcí | kapacita | min. cena | váha1 | cena1 | váha2 | cena2 | ... | ... |
| -- | ---------- | -------- | --------- | ----- | ----- | ----- | ----- | --- | --- |

- id - id batohu
- počet věcí - počet věcí na vstupu
- kapacita -  maximální hmotnostní kapacita batohu
- min. cena - požadovaná minimální cena
- váha $`i`$ - váha $`i`$-té položky
- cena $`i`$ - cena $`i`$-té položky

Všechna vstupní data jsou nezáporná celá čísla. Každý záznam je ukončen novou řádkou.

## Úkol 

Vaším úkolem je rozhodnout, zda je možné do batohu poskládat položky tak, aby součet jejich vah nepřekročil kapacitu a zároveň součet jejich cen dosahoval alespoň minimální ceny. 

Výstup budete zapisovat do souboru ve formátu: 

   | id  | 0/1 |
   | --  | --- |

- id - id batohu
- 0/1 - pokud lze vytvořit batoh alespoň s minimální cenou, do souboru zapíšete 1, jinak 0. 

Každý záznam bude ukončen novým řádkem.

Názvy souborů dostanete předané při volání programu jako jeho argumenty.

## Testovací data 
- ```input.txt``` - vzorová vstupní data
- ```output.txt``` - vzorová výstupní data pro soubor ```input.txt```

## Nápověda

* Argumenty programu se předávají do funkce main jako argc a argv. Nultý prvek pole argv je název programu.
* Zatím neřešte správnost formátu dat ani řádkování vstupu. 
* Nechceme po Vás optimální algoritmus. Stačí nám řešení hrubou silou.
* Jde o rozhodovací problém. Nehledáme optimální naplnění batohu, pouze rozhodujeme, zda jej vůbec lze naplnit.
* Úloha jasně vede na rekurzi. V průběhu algoritmu: O části předmětů už jsem rozhodl (resp. to zkouším), zda je dám do batohu nebo ne; nyní musím určit, zda se mi do batohu může vejít zbytek věcí tak, že bude splněna minimální cena věcí v batohu, když:
  * a) dám do batohu následující předmět,
  * b) nedám do batohu následující předmět.
* Jinými slovy: při řešení batohu budu zjišťovat, zda se mi zbytek věcí nevejde do trochu menšího batohu s trochu jiným požadavkem na cenu.
