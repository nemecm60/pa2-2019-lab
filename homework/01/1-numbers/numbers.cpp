#include <cassert>

#include <initializer_list>
using dlist = const std::initializer_list<int>&;

struct ListNode {
    int val;
    ListNode *next;

    ListNode(int x)  {
        val = x;
        next = nullptr;
    }
};

ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {

    /* TODO */

}

// test helper functions

ListNode* createNumber(dlist digits) {
    ListNode* result = nullptr;
    for(auto d : digits) {
        auto r = new ListNode(d);
        r->next = result;
        result = r;
    }
    return result;
}

bool equalNumbers(ListNode* a, ListNode* b) {
    return ((a ? a->val : 0) == (b ? b->val : 0)) && ((!a && !b) || equalNumbers(a ? a->next : a, b ? b->next : b));
}

void freeNumber(ListNode* n) {
    if(n) {
        freeNumber(n->next);
        delete n;
    }
}

bool testAddition(dlist digitsA, dlist digitsB, dlist digitsResult) {
    auto a = createNumber(digitsA);
    auto b = createNumber(digitsB);
    auto result = createNumber(digitsResult);
    auto ab = addTwoNumbers(a, b);
    auto a2 = createNumber(digitsA);
    auto b2 = createNumber(digitsB);
    bool decision = equalNumbers(ab, result) && equalNumbers(a, a2) && equalNumbers(b, b2);
    freeNumber(a);
    freeNumber(b);
    freeNumber(result);
    freeNumber(ab);
    freeNumber(a2);
    freeNumber(b2);
    return decision;
}

int main() {
    assert(testAddition({1},{2},{3})); // 1 + 2 = 3
    assert(testAddition({1},{9},{1,0})); // 1 + 9 = 10
    assert(testAddition({7},{4},{1,1})); // 7 + 4 = 11
    assert(testAddition({8,7},{4,5},{1,3,2})); // 87 + 45 = 132
    assert(testAddition({9,9,9,9},{1},{1,0,0,0,0})); // 9999 + 1 = 10000
    assert(testAddition({1},{9,9,9,9},{1,0,0,0,0})); // 1 + 9999 = 10000
    assert(testAddition({1},{9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9},{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}));
}
