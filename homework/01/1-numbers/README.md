# Spojový seznam

Vaším úkolem je naprogramovat funkci 
	
```c++
ListNode* addTwoNumbers(ListNode* l1, ListNode* l2)
```

Funkce dostane jako parametry dva spojové seznamy, reprezentující dvě přirozená čísla v desítkové soustavě. Jednotlivé číslice daného čísla jsou uloženy ve spojovém seznamu v reverzním pořadí, tedy od jednotek k vyšším řádům. Každý uzel spojového seznamu obsahuje právě jednu číslici.
 
Návratovou hodnotou funkce je nový spojový seznam, který reprezentuje součet daných dvou čísel.

Původní seznamy musí zůstat nedotčené.

## Příklad:

Vstup: (2 -> 4 -> 3) + (5 -> 6 -> 4)

Výstup: 7 -> 0 -> 8

Význam: 342 + 465 = 807.

## Nápověda

* Úloha jasně vede na rekurzi.
* Jak si možná pamatujete z logických obvodů, dvě čísla ve sčítací funkci Vám pravděpodobně nebudou stačit. Nebojte se definovat si vhodnější sčítací funkci podle svých potřeb.
* Zdrojový kód testovací části obsahuje některé pokročilé konstrukty jazyka C++. V průběhu semestru se ke všem dostaneme, teď se jich nebojte.
