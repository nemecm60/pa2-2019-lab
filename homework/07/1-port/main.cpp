#include<iostream>
#include<string>

using namespace std;

class Port {
private:
    string brand;
    string style;
    int bottles;
public:
    Port(const string& br = "none", const string& st = "none", int b = 0);
    Port(const Port& p); // copy ctor
    Port& operator=(const Port& p);
    Port& operator+=(int b); // adds b bottles
    Port& operator-=(int b); // removes b bottles
    int getBottles() const;
    virtual void print() const; // print information in format: Brand: , Style: , Bottles:
    friend ostream& operator<<(ostream &os, const Port& p);
};

// style must have value vintage
class VintagePort: public Port {
private:
    string nickname;
    int year;
public:
    VintagePort();
    VintagePort(const string& br, int b, const string & nn, int y);
    VintagePort(const VintagePort& vp);
    VintagePort& operator=(const VintagePort& vp);
    void print() const;
    friend ostream& operator<<(ostream &os, const VintagePort& vp);
};

int main() {

}