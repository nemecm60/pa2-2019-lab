# Vinařství


Vaším úkolem bude dokončení třídy `VintagePort`:
   * Vytvořte definice moted třídy Port. [1b]
   * Vysvětlete proč jsou některé metody předdefinované a jiné ne. [0.5b]
   * Vysvětlete proč jsou funkce operator=() a operator<<() virtuální. [0.5b]
   * Vytvořte definice metod třídy VintagePort. [1b]