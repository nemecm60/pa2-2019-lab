# Čas

Vaším úkolem bude implementovat funkce definované ve třídě `Time` .

1. `Time(int h, int min, int sec)` 
    * Konstruktor pro vytvoření objektu s nastavenými `hours`, `minutes` a `seconds`.
    
2. `void AddHr(int h)`
    * Přičte h hodin k hours.   
      

3.  `void AddMin(int m)` 
    * Přičtěte `m` minut k `minutes`.
    * Minuty musí být ve validním formátu. Pokud je minutes >= 60, převeďte na hodiny.   
   
4. `void  AddSec(int s)`
    * Přičtěte `s` sekund k `seconds`.
    * Sekundy musí být ve validním formátu. Pokud je seconds >= 60,  převeďte na minuty, případně na hodiny.   

5. `void Reset(int h, int m, int sec );`
    * Nastaví čas na příslušné hodnoty.

Dále do třídy `Time` přidejte:
1. Operátor sčítání: 
    * Time t = time1 + time2 
    
2. Operátor odčítání:
    * Time t = time1 - time2 
    * Pokud time2 bude větší než time1, t bude mít nastavené vše na nulu.
      

3.  Operátor násobení:
    * Time t = time1*x; 
    * Vynásobení všech časových údajů číslem `x`.
        

## Organizace

Zadání vyřešte do středy 25. března. Řešení posílejte na mail jan.matousek@fit.cvut.cz, nebo marikja7@fit.cvut.cz, nebo vystavte ve svém repozitáři na gitlabu (a upozorněte nás). 
