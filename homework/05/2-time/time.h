#ifndef PA2_2019_LAB_TIME_H
#define PA2_2019_LAB_TIME_H

class Time {
private:
    int hours;
    int minutes;
    int sec;
public:
    Time();
    Time(int h, int min = 0, int sec = 0);
    void AddHr(int h);
    void AddMin(int m);
    void AddSec(int s);
    void Reset(int h = 0, int m = 0, int sec = 0 );
    // TODO: add operator +
    // TODO: add operator -
    // TODO: add operator *

};


#endif //PA2_2019_LAB_TIME_H
