# String

Uvažujme třídu String:
    1. Co je špatně na tomto implicitním konstruktoru?
    ```c++
        String::String() {}
    ```
    2. Co je špatně na tomto  konstruktoru?
    ```c++
        String::String(const char*s) {
            str = s;
            len = strlen(s);
        }
    ```
    3. Co je špatně na tomto konstruktoru?
    ```c++
        String::String(const char*s) {
            strcpy(str, s);
            len = strlen(s);
        }
    ```

Do implementujte kopírovací konstruktor a operátor přiřazení pro třídu `String`.      
    
## Organizace

Zadání vyřešte do středy 25. března. Řešení posílejte na mail jan.matousek@fit.cvut.cz, nebo marikja7@fit.cvut.cz, nebo vystavte ve svém repozitáři na gitlabu (a upozorněte nás). 
