#ifndef PA2_2019_LAB_STRING_H
#define PA2_2019_LAB_STRING_H

class String {
    char* str; // points to string created by operator new
    int len; // lenght of string str
public:
    String();
    String(const char*);
};

#endif //PA2_2019_LAB_STRING_H
