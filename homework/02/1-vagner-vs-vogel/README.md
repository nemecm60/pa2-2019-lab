# Vagner vs Vogel

_Damstvo a panstvo, souboj titánů titánovitých titanových titaničitých... co to sakra plácám, no prostě Vašich oblíbených učitelů, je zde! Hero Vogel a Boss Vagner se do sebe pustili, a Ty, můj milý studente, Ty nejsi náhodný divák, Ty jsi strůjce tohoto souboje._

_Vy si totiž tento souboj naprogramujete, protože proč sakrapráce ne, nejste přece žádní šupáci._

-- Jak by asi prezentoval zadání jistý nejmenovaný herní publicista, kdyby učil PAx.

## Zadání

1. Vytvořte strukturu `hero`, která bude evidovat jméno hrdiny (stačí `const char *`, ale `std::string` je povolen také), hitpointy, útočné číslo a obranné číslo.

   * Nadefinujte strukturu a její položky.
   * Napište vhodný konstruktor.
   * Ve funkci `main` vytvořte dvě instance, jedna bude Hero Vogel, druhá bude Boss Vagner. Hodnoty dalších parametrů si vymyslete.  

2. Napište metodu `print` pro výpis informací o hrdinovi (jako argument dostane referenci na `std::ostream`, tento využije při výpisu).

   * Metodu použijte ve funkci `main` pro představení hrdinů.
   
3. Napište metodu (vhodně ji pojmenujte), která vrátí, zda je hrdina živý (hrdina je mrtvý, pokud má 0 nebo méně hitpointů).

   * Tuto metodu použijte ve výpisu informací o hrdinovi.

4. Napište metodu pro útok. Parametrem metody je napadený hrdina, na kterého se útočí; `this` ukazuje na útočníka.

   * Metoda porovná útočné číslo útočníka s obranným číslem obránce; je-li vyšší, útok uspěl, a obránce ztratí tolik hitpointů, kolik je rozdíl mezi útokem a obranou.
   * Přidejte další parametr pro výpis akce na výstup.
   
5. Do funkce `main` přidejte samotný souboj. Hrdinové se střídají, začíná Hero Vogel.

   * Souboj končí zabitím jednoho nebo druhého hrdiny.
   * Na konci vypište, kdo vyhrál.
   
## Rozšíření (za každé další virtuální bod za aktivitu)

1. Vytvořte strukturu `dice` pro hrací kostku, a použijte ji při souboji.

   * Kostka bude mít metodu `cast`, která vrátí náhodné celé číslo od 1 do 6.
   * _Vnitřní řešení kostky není důležité, může klidně házet stále stejné číslo._
   * Ukazatel na hrací kostku vložte hrdinům při konstrukci.
   * Při souboji použijte kostku k navýšení útočeného i obranného čísla.
   * Implementujte pro kostku metodu `stats`, s jejíž pomocí na konci souboje vypište, kolikrát padlo které číslo.

2. Přidejte metody pro uložení a načtení dat o hrdinovi do binárního souboru.

   * Uložení bude realizovat metoda `save`, která přijme název souboru.
   * Pro načtení hrdiny zvolte vhodnou metodu sami. 
   * Vytvořte 2 soubory pro hrdiny, a hrdiny místo inicializace ve funkci `main` načtěte ze souboru.
   * Přidejte podporu pro dva argumenty programu (viz. `argc` a `argv`) s názvy souborů hrdinů.

## Organizace

Zadání vyřešte do středy 4. února. Řešení posílejte na mail jan.matousek@fit.cvut.cz, nebo marikja7@fit.cvut.cz, nebo vystavte ve svém repozitáři na gitlabu (a upozorněte nás). 
