# Evaluate Division


Vaším úkolem bude implementovat funkci `calcEquation`. Jako vstup funkce `calcEquation` dostanete:
   * rovnice ve formátu `A / B = k`, kde `A` a `B` jsou proměnné reprezentované jako string a `k` je reálné číslo.
   * hodnoty
   * dotazy.

Úkolem je správně vyhodnotit dané dotazy, pokud řešení pro dotaz neexistuje, uložte -1.0.

Příklad:
* Rovnice: `a / b = 2.0, b / c = 3.0.`
* Dotazy: `a / c = ?, b / a = ?, a / e = ?, a / a = ?, x / x = ? .`
* return `[6.0, 0.5, -1.0, 1.0, -1.0 ]`.

Vstup korespondující s předchozím příkladem, vypadá takto:

* rovnice = `[ ["a", "b"], ["b", "c"] ]`,
* hodnoty = `[2.0, 3.0]`,
* dotaty = `[ ["a", "c"], ["b", "a"], ["a", "e"], ["a", "a"], ["x", "x"] ]`.
