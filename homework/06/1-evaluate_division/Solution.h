#pragma once
#include <iostream>
#include <vector>
#include <string>

class Solution {

public:
    std::vector<double> calcEquation(std::vector<std::vector< std::string>>& equations,
                                        std::vector<double>& values, std::vector<std::vector<std::string>>& queries);
};

