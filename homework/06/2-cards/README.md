# Cards

Alice má na ruce kary dané jako pole celých čísel. Chce si přeuspořádat karty do `W `skupin tak,
aby každá skupina měla velikost `W` a byla rostoucí.

Pokud tak může učinit, vraťte `true`, jinak `false`.
 
Příklad 1:

* Input: `hand = [1,2,3,6,2,3,4,7,8]`, `W = 3`
* Output: `true`
    * Alice může karty zorganizovat:` [1,2,3],[2,3,4],[6,7,8]`.

Příklad 2:

* Input: `hand = [1,2,3,4,5]`, `W = 4`
* Output: `false`