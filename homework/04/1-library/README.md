# Knihovna

Vaším úkolem bude implementovat funkce ve třídě `Library` a `Book`.

Třída `Library` obsahuje :

1. `addBook(const std::string& author, const std::string& title, int id)` 
    * Pokud ve vectoru `books` již kniha se stejnou dvojci `author`, `title` nebo se stejným `id` existuje, funkce vrátí false.
    * Jinak knihu přidá do seřazeného vectoru na vhodnou pozici (vector je řazen podle id) a vrátí true.   
 
2. `removeBook(const std::string& author, const std::string& title)`
    * Pokud ve vectoru `books` kniha se stejnou dvojicí author, title neexistuje, vrátí false.
    * Jinak knihu odebere a vrátí true.   
      

3.  `removeBook(int id)` 
    * Pokud ve vectoru `books` kniha se stejným id neexistuje, vrátí false.
    * Jinak knihu odebere a vrátí true.   
   
4. `findBook(const std::string& author, const std::string& title)`
    * Vrátí knihu se stejným `author` a `title`.

5. `findBook(int id)`
    * Vrátí knihu se stejným `id`.

## Rozšíření (za každé další virtuální bod za aktivitu)

1. Doplňte `const` a `&` na vhodná místa. 

2. Definice tříd a implementace metod přesuňte do odlišných souborů. 


## Organizace

Zadání vyřešte do středy 18. února. Řešení posílejte na mail jan.matousek@fit.cvut.cz, nebo marikja7@fit.cvut.cz, nebo vystavte ve svém repozitáři na gitlabu (a upozorněte nás). 
