#include <iostream>
#include <cassert>
#include <string>
#include <vector>
#include <algorithm>


// TODO : add const and & on proper place

class Book {

    Book(std::string author,  std::string title, int id) {
        // TODO create book with author and title
    }

private:
    int id;
    std::string author;
    std::string title;
};

class Library {
public:

    bool addBook( std::string author, std::string title, int id) {
        // TODO Add Book to a proper place in the sorted vector (vector is sorted by id) and return true.
        //      If book already exits, return false.
    }

    bool removeBook( std::string author, std::string title) {
        //TODO Remove book with same author and title from the vector and return true.
        //     If there is no match, return false.
    }

    bool removeBook(int id) {
        //TODO Remove book with same id from the vector and return true.
        //     If there is no match, return false.
    }

    Book findBook( std::string author, std::string title) {
        //TODO Find book with same author and title from vector.
    }

    Book findBook(int id) const {
        //TODO Find book with same id from the vector.
    }
private:
    // sorted vector of books by id
    std::vector<Book> books;
};

int main() {

}