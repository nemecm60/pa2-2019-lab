# Bankovní účet

1. Vytvořte třídu `Account`, která bude evidovat:

   * jméno majitele úču (stačí `char *`, ale `std::string` je povolen také).
   * peněžní zůstatek.
   * historii transakcí - eviduje každý vklad (kladné číslo) a výběr (záporné číslo).  

2. Vytvořte konstruktor, který vytvoří prázdný účet. 

3. Vytvořte druhý konstruktor, který dostane jako parametry číslo účtu, jméno majitele účtu a zůstatek. 

4. Vytvořte destruktor, který destruuje pole transakcí a jméno majitele účtu.

5. Napište metodu, která vloží nebo vybere peníze z účtu. Záznam uložte do historie transakcí.
Pokud peněžní zůstatek neumožňuje danou transakci provést, informujte uživatele.

6. Vytvořte metodu, která vypíše informace o `Account`. Je na vás v jakém formátu.

7. Vytvořte metodu, která seřadí historii transakcí podle velikosti částky. 
Můžete použít knihovní funkci quicksort.
 
Poznámka: Používejte const a reference!

## Rozšíření (za každé další virtuální bod za aktivitu)

1. Vytvořte třídu `Person`, která bude obsahovat:

   * jméno majitele úču (stačí `char *`, ale `std::string` je povolen také).
   * rok narození.
   * id osoby.  
   
   * konstruktor, destruktor.
   
   Jméno majitele účtu ve třídě `Account` nahraďte prvkem typu Person. 
   Nezapomeňte vhodně upravit konstruktor a destruktor třídy  `Account`.

2. Definice tříd a implementace metod přesuňte do odlišných souborů.
 


## Organizace

Zadání vyřešte do středy 11. února. Řešení posílejte na mail jan.matousek@fit.cvut.cz, nebo marikja7@fit.cvut.cz, nebo vystavte ve svém repozitáři na gitlabu (a upozorněte nás). 
