# Úlohy na samostatnou práci

Zde se nacházejí úlohy na samostatnou práci. Za jejich vypracování do následujícího pondělí získáte body za aktivitu.

## Jak zasílat řešení

Řešení realizujte ve svých repozitářích, v nich poté vytvořte merge request a pošlete nám na něj odkaz.