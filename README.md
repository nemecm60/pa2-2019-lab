# Laboratorní cvičení BI-PA2 LS 2019/2020

V tomto repozitáři se nacházejí všechny materiály, se kterými pracujeme na cvičeních PA2. Budou sem také umisťovány výsledné programy, a také zadání samostatné práce a bug huntů.

Obsah jednotlivých složek je popsán v příslušných README souborech ve složkách.

## Cvičící:

* Jan Matoušek (matouj10@fit.cvut.cz)
* Jana Maříková (marikja7@fit.cvut.cz)

## Další informace

* Cvičení probíhá každý čtvrtek od 7:30 do 9:00, s výjimkou 30.4.
* Stránky předmětu: https://courses.fit.cvut.cz/BI-PA2/index.html
* Stránky cvičících: https://courses.fit.cvut.cz/BI-PA2/teacher/matouj10/index.html

## Struktura repozitáře

V repozitáři jsou vytvořené následující větve:

* `master` -- bude obsahovat zadání všech cvičení; před každým cvičením (po finalizaci podkladů pro cvičení) bude v `master` větvi vytvořen nový _tag_ s názvem `labXX`.
* `solution` -- pro všechna proběhnutá cvičení; sem budeme přidávat veškeré úpravy provedené na cvičeních na projektoru, a později i naše řešení úloh na samostatnou práci

Větev `solution` nebude nikdy mergnutá do větve `master`, můžete si tedy kdykoliv repozitář stáhnout (i ve zkouškovém období) a sami si řešit úlohy, aniž byste se museli vracet v historii repozitáře.

Jednotlivá cvičení jsou rozdělená do složek lab, homework a bug-hunt. Případný teoretický výklad je obvykle součástí složky lab, většina teorie je však v přednáškách nebo na stránkách předmětu.

## Jak pracovat s tímto repozitářem

Repozitář si forkněte do svého soukromého profilu na fakultním Gitlabu. Jeho viditelnost nastavte jako private, a pozvěte nás (matouj10 a marikja7) jako osoby v roli reporter. Vaši privátní kopii repozitáře si potom otevřte ve Vašem oblíbeném IDE. Např. v CLion přes nabídku _VCS - Get from Version Control..._

K práci v repozitáři si vytvořte nějakou vlastní větev, např. `my-solution`, tuto si před každým cvičením rebasněte.

Ve svém IDE provádějte změny podle svého uvážení. Změny poté nahrajte (commit & push) do svého repozitáře na fakultním gitlabu. Poté vytvořte merge request této větve (pustí Vás to jen v rámci Vašeho repozitáře, do tohoto nemáte přístup, protože současná verze Gitlabu nepodporuje soukromé merge requesty), a pozvěte nás do něj, abychom se na něj podívali a mohli Vám poskytnout zpětnou vazbu přímo k Vašemu zdrojovému kódu.

Pokud si s gitem a Gitlabem ještě příliš netykáte, můžete s repozitářem pracovat postaru - stáhněte si jej jako zip, rozbalte a pracujte; Vaše zdrojáky nám pak posílejte mailem. Sice nebudeme schopni vést jednoduše diskusi nad jednotlivými řádky kódu, ale budeme se snažit i tak.

Doporučujeme Vám však, abyste se s gitem naučili. V praxi se git (případně jiný verzovací nástroj) používá velmi často. Na FITu sice existuje volitelný předmět, ale základní práce se dá naučit i bez něj; navíc většina rozumných IDE (včetně Gitlabu samotného) Vás od textových příkazů odstíní, a pak se stačí naučit akorát základní koncepty jako staging, commit (revize), tag (štítek, značka), branch (větev), merge (sloučení větví), rebase (přemístění větve na jiný kořen) a push/pull (synchronizace s jiným repozitářem). Pokud se vyhnete provádění změn ve větvích `master` a `solution`, neměli byste s tímto repozitářem narazit na problém.

Existuje i pěkná knížka, která je zdarma, a dokonce v češtině:

https://git-scm.com/book/cs/v2 

Specificky pro CLion (a další nástroje od Jetbrains) si potom můžete nastudovat uživatelskou příručku, kapitolu Version control / Git, ale doporučuji projít i kapitoly Review changes, Shelve and Unshelve Changes a Resolve Conflicts.

https://www.jetbrains.com/help/clion/using-git-integration.html

## Jak kompilovat

V repozitáři je nachystána veškerá struktura pro kompilaci ve vývojovém prostředi CLion, případně i v jiných prostředích, které pracují se systémem `cmake`. Každý program má svůj target, který stačí vybrat v menu, zkompilovat a spustit.

Je však dobré umět kompilovat i ručně. Pro C++ verze C++14 stačí kompilátor spustit takto:

```
g++ -Wall -pedantic -std=c++14 program.cpp -o program
```

Samozřejmě pro potřeby ladění se hodí ještě přepínač `-g`, a pokud se rozhodnete neladit, ale zkompilovat finální produkt, použijte přepínač `-O3` pro nejvyšší stupeň optimalizace.

Pokud používáte CLion, je vhodné si v nastaveních vyhledat CMake a přidat nový profil - automaticky se přidá profil _Release_, který umožní ostrou kompilaci včetně optimalizace.
